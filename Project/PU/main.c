/**
  ******************************************************************************
  * @file    Project/main.c 
  * @author  MCD Application Team
  * @version V2.2.0
  * @date    30-September-2014
  * @brief   Main program body
   ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 


/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "main.h"
#include "periph_init.h"
#include "bluetooth_control.h"
#include "public.h"
    
PuDataTypeDef PuData = 
{
  .CurrentAdcConvChan = LL_PU_ADC_CHANNEL,  
  .UserCommand = COMMAND_NONE,
  .Controller = CONTROLLER_NONE,
  .ObjHoldState = OBJ_HOLD_UNKNOWN,
  .Uart3BytesRxNum = 0,
  .Uart1BytesRxNum = 0, 
  .OnBusyUserCommand = COMMAND_NONE
};

/* Private defines -----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


static void PuDataInit(PuDataTypeDef *puData);                                  //данные ПУ


void main(void)
{
  ClockInit();                                                                  //инициализация тактирования
  GpioInit();                                                                   //инициализация GPIO
  BeeperInit();                                                                 //инициализация бипера
  UART1_Config();                                                               //инициализация UART1
  UART3_Config();                                                               //инициализация UART3
  ADC_Config();                                                                 //инициализация АЦП
  //инициализация таймеров
  TIM4_Config();
  TIM2_Config();
  TIM3_Config();  
  TIM1_Config();
  
  Exti_Init();                                                                  //инициализация внешнего прерывания
  PuDataInit(&PuData);                                                          //инициализация данных ПУ
  
  enableInterrupts();                                                           //включение прерываний
  
  ConfigMpkBluetoothModule(&PuData);                                            //конфигурирование модуля для связи с МПК
  ConfigSmartphoneBluetoothModule(&PuData);                                     //конфигурирование модуля для связи с смартфоном
  
  TIM2_Cmd(ENABLE);                                                             //Пуск таймера АЦП
   
  GetPlatformPosition(&PuData);                                                 //получение положения платформы
  if(CheckStatState() == TRUE)
  {
    EnablePowerChannel2();                                                      //включить канал 2 
  } 
  uint8_t portState = GPIO_ReadInputData(PLUS_MINUS90DEG_PORT);                 //получить состояние пинов порта, к которому подключен тумблер   
  GetOperatorToggleSwitchState(&PuData,portState);
  /* Infinite loop */
  while (1)
  {
    MainWork(&PuData);                                                          //основной цикл работы  
  }
  
}

//инициализация данных ПУ
void PuDataInit(PuDataTypeDef *puData)
{
  SetState(puData,STATE_IDLE);  
  
}

//установка состояния автомата
void SetState(PuDataTypeDef *puData,PuStateTypeDef newState)
{
  puData->PrevState = puData->State;                                            //сохранение предыдущего сотояния
  puData->State = newState;                                                     //установка нового состояния
}

//возврат к предыдущему состоянию автомата
void GoToPreviousState(PuDataTypeDef *puData)
{  
  puData->State = puData->PrevState;  
}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
