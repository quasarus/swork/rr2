#include "main.h"
#include "periph_init.h"
#include "stm8s.h"
#include "pu_work.h"
#include "bluetooth_control.h"
#include "motor_control.h"
#include "public.h"

//static void RotatePlatform(PuDataTypeDef *puData);
static void StartCommandExecution(PuDataTypeDef *puData);
static void GetRotatePlatformToZeroData(PuDataTypeDef *puData);
static void IndicateUnknownPlatformPosition();
static void FreeObjectOnMainPowerOff(PuDataTypeDef *puData);
//цикл работы ПУ
void MainWork(PuDataTypeDef *puData)
{     
  
  switch(puData->State)                                                         //автомат состояний
  {
    case STATE_IDLE:                                                            //бездействие
    {      
      puData->CurrentAdcConvChan = LL_PU_ADC_CHANNEL;                           //установка канала АЦП - измерение света
      puData->Controller = CONTROLLER_NONE;                                     //сброс управляющего  
      GPIO_WriteLow(HL2_LED_PORT,HL1_HL2_LED_PIN);
      if(puData->OnBusyUserCommand != COMMAND_NONE)
      {
        SetCommand(puData,puData->OnBusyUserCommand,CONTROLLER_OPERATOR);
        puData->OnBusyUserCommand = COMMAND_NONE;        
      }
      break;
    }
    case STATE_CATCHING_OBJECT:                                                 //состояние процесса захвата объекта
    {
      
      break;
    }
    case STATE_REALEASING_OBJECT:                                               //состояние процесса освобождения объекта
    {
            
      break;
    }
    case STATE_START_CATCH_OBJECT:                                              //состояние начала захвата объекта
    {
      
      EnablePowerChannel1();                                                    //включить питание 1го канала
#ifdef USE_BEEPER
      BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);        
      Delay(500);      
      BEEP_Cmd(DISABLE);
      Delay(500);
      BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);        
      Delay(500);      
      BEEP_Cmd(DISABLE);
#else
      Delay(1500);                                                              //задержка перед захватом
#endif
      
                                                                    
      puData->CurrentAdcConvChan = OVERLOAD_PU_ADC_CHANNEL;                     //установка измерения АЦП по каналу перегрузки мотора      
      M2StartCatch();                                                           //запустить мотор на захват
      SetState(&PuData,STATE_CATCHING_OBJECT);                                  //перейти в состояние процесса захвата объекта
      break;
    }   
    case STATE_START_REALEASE_OBJECT:                                           //состояние начала освобождения объекта
    {
      puData->CurrentAdcConvChan = OVERLOAD_PU_ADC_CHANNEL;                     //установка измерения АЦП по каналу перегрузки мотора      
      M2StartFree();                                                            //запустить мотор на освобождение
      
      SetState(&PuData,STATE_REALEASING_OBJECT);                                //перейти в состояние процесса освобождения объекта   
      break;
    }    
    case STATE_RELEASED_OBJECT:                                                 //состояние после освобождения объекта
    {  
      puData->ObjHoldState = OBJ_IS_RELEASED;                                   //сохранение текущего состояния удержания объекта    
      SetState(&PuData,STATE_IDLE);                                             //переход в состояние бездействия
      break;
    }
    case STATE_HOLD_OBJECT:
    {
      puData->ObjHoldState = OBJ_IS_HOLD;                                       //сохранение текущего состояния удержания объекта
      SetState(&PuData,STATE_IDLE);                                             //переход в состояние бездействия
      break;
    }    
    case STATE_EXEC_NEW_COMMAND:                                                //состояние начала выполнения новой команды
    {
      GPIO_WriteHigh(HL2_LED_PORT,HL1_HL2_LED_PIN);
      StartCommandExecution(puData);            
      break;
    }
    case STATE_ROTATE_PLATFORM:                                                 //состояние продолжения вращения платформы
    {      
      ContinueRotatePlatform(puData);    
      break;
    }
    case STATE_SEND_ANSWER_PU_NO:                                               //состояние отправки отрицательного ответа на запрос, является ли канал каналом для связи с МПК
    { 
      SendAnswerAsPuSlaveForSmartphone(BT_COMMAND_IS_PU_ANSWER_NO);
      GoToPreviousState(puData);
      break;
    }
    case STATE_SEND_ANSWER_PU_YES:                                              //состояние отправки положительного ответа на запрос, является ли канал каналом для связи с МПК
    { 
      SendAnswerAsPuSlaveForMpk(BT_COMMAND_IS_PU_ANSWER_YES);
      GoToPreviousState(puData);      
      break;
    }
  }  
}


//старт выполнения команды
static void StartCommandExecution(PuDataTypeDef *puData)
{
  
  
  switch(puData->UserCommand)
  {    
    case COMMAND_PLUS90DEG:                                                     //если поворот платформы к +90
    {
      if(puData->PlatformState == PLATFORM_POS_PLUS90DEG)                       //если платформа уже в состоянии +90, перейти в бездействие
      {
        SetState(&PuData,STATE_IDLE);
        break;
      }
#ifdef USE_BEEPER
      BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);        
      Delay(250);      
      BEEP_Cmd(DISABLE);
#endif
      puData->EndPlatformState = PLATFORM_POS_PLUS90DEG;                        //задать конечной точкой положение +90
      puData->IsClockwiseRotate = FALSE;                                        //флаг - вращать платформу против часовой
      SetState(&PuData,STATE_ROTATE_PLATFORM);                                  //перейти в состояние вращения платформы 
      break;
    }
    case COMMAND_MINUS90DEG:                                                    //если поворот платформы к -90
    {
      if(puData->PlatformState == PLATFORM_POS_MINUS90DEG)                      //если платформа уже в состоянии -90, перейти в бездействие
      {
        SetState(&PuData,STATE_IDLE);
        break;
      }
#ifdef USE_BEEPER
      BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);        
      Delay(250);      
      BEEP_Cmd(DISABLE);
#endif
      puData->EndPlatformState = PLATFORM_POS_MINUS90DEG;                       //задать конечной точкой положение -90
      puData->IsClockwiseRotate = TRUE;                                         //флаг - вращать платформу по часовой
      SetState(&PuData,STATE_ROTATE_PLATFORM);                                  //перейти в состояние вращения платформы
      break;
    }
    case COMMAND_RETURN0:                                                       //выполнить команду возврата в 0
    {      
      GetRotatePlatformToZeroData(puData);
      break;
    }
    case COMMAND_CATCH_OBJECT:                                                  //выполнить команду захвата объекта
    {
      SetState(&PuData,STATE_START_CATCH_OBJECT);      
      break;
    }
    case COMMAND_FREE_OBJECT:
    {
      SetState(&PuData,STATE_START_REALEASE_OBJECT);                            //выполнить команду освобождения объекта      
      break;
    }
    case COMMAND_NONE:
    {      
      break;
    }    
    default:
      break;
  }
}

//выполнить команду возврата в 0
void GetRotatePlatformToZeroData(PuDataTypeDef *puData)
{
  
  switch(puData->PlatformState)
  {    
    case PLATFORM_POS_MINUS90DEG:                                               //если платформа в позиции -90
    {
#ifdef USE_BEEPER
      BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);        
      Delay(250);      
      BEEP_Cmd(DISABLE);
#endif
      puData->EndPlatformState = PLATFORM_POS_ZERO;                             //задать конечной точкой положение 0
      puData->IsClockwiseRotate = FALSE;                                        //флаг - вращать платформу против часовой
      SetState(&PuData,STATE_ROTATE_PLATFORM);                                  //перейти в состояние вращения платформы
      break;
    }
    case PLATFORM_POS_PLUS90DEG:
    {
#ifdef USE_BEEPER
      BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);        
      Delay(250);      
      BEEP_Cmd(DISABLE);
#endif
      puData->EndPlatformState = PLATFORM_POS_ZERO;                             //задать конечной точкой положение 0
      puData->IsClockwiseRotate = TRUE;                                         //флаг - вращать платформу по часовой
      SetState(&PuData,STATE_ROTATE_PLATFORM);                                  //перейти в состояние вращения платформы
      break;
    }
    case PLATFORM_POS_ZERO:                                                     //если платформа уже в позиции 0 - перейти в бездействие
    {
      SetState(&PuData,STATE_IDLE);
      break;
    }    
    default:
      break;
  }
}




//установка команды
void SetCommand(PuDataTypeDef *puData,UserCommandTypeDef command,PuControllerTypeDef controller)
{
  //сброс счетчика миллисекундной задержки
  TimingDelay = 0;
  //игнорировать команду, если приоритет управляющего, задающего команду меньше предыдущего управляющего, задавшего команду
  if((uint8_t)controller < (uint8_t)puData->Controller)
  {
    return;
  }
  
  if(puData->State != STATE_IDLE)
  {
    puData->OnBusyUserCommand = command;    
    return;
  }
  
  puData->Controller = controller;                                              //сохранение управляющего
  puData->UserCommand = command;                                                //сохранение команды
  SetState(puData,STATE_EXEC_NEW_COMMAND);  
}

//по прерыванию от датчиков положения платформы
void OnPosSensorInterrupt()
{  
  GetPlatformPosition(&PuData);                                                 //получить позицию платформы    
}

//получить позицию платформы
bool GetPlatformPosition(PuDataTypeDef *puData)
{
  //получить состояние пинов датчиков
  uint8_t posSensorsState = GPIO_ReadInputData(GPIOE) & POS_SENSORS_GPIO_MASK;  
  switch(posSensorsState)
  {
    
    case SENSOR_POS90_PIN_MASK:
    {
      puData->PlatformState = PLATFORM_POS_PLUS90DEG;      
      break;
    }
    case SENSOR_MINUS90_PIN_MASK:
    {
      puData->PlatformState = PLATFORM_POS_MINUS90DEG;
      break;
    }
    case SENSOR_ZERO_PIN_MASK:
    {
      puData->PlatformState = PLATFORM_POS_ZERO;
      break;
    }
    default:
      IndicateUnknownPlatformPosition();                                        //при неизвестной позиции платформы - индикация     
      puData->PlatformState = PLATFORM_POS_UNKNOWN;      
      return FALSE;    
  }
  return TRUE;
}


//при переключении оператором тублера установить команду
void GetOperatorToggleSwitchState(PuDataTypeDef *puData,uint8_t portState)
{
  portState &= PLUS_MINUS90DEG_PINS;
  switch(portState)
  {
    case PLUS_MINUS90DEG_PINS:
    {
      SetCommand(puData,COMMAND_RETURN0,CONTROLLER_OPERATOR);      
      break;
    }
    case PLUS_90DEG_PIN:
    {
      //GPIO_WriteHigh(HL1_LED_PORT,HL1_HL2_LED_PIN);      
      SetCommand(puData,COMMAND_MINUS90DEG,CONTROLLER_OPERATOR);
      break;
    }
    case MINUS_90DEG_PIN:
    {
      //GPIO_WriteHigh(HL2_LED_PORT,HL1_HL2_LED_PIN);
      SetCommand(puData,COMMAND_PLUS90DEG,CONTROLLER_OPERATOR);
      break;
    }
  }
}


//по прерыванию от тумблера
void OnExtPortDInterrupt()
{
  uint8_t portState = GPIO_ReadInputData(PLUS_MINUS90DEG_PORT);                 //получить состояние пинов порта, к которому подключен тумблер   
  GetOperatorToggleSwitchState(&PuData,portState);
}

//проверить состояние основного питания
bool CheckStatState()
{
  uint8_t portState = GPIO_ReadInputData(STAT_PORT);  
  if((portState & (uint8_t)STAT_PIN) > 0)                                       //при отсутствии питания основного канала
  {       
    return TRUE;
  }
  return FALSE;
}

//при прерывании на датчике основного питания
void OnStatInterrupt()
{
  if(CheckStatState() == TRUE)                                                  //проверить состояние основного питания 
  {
    EnablePowerChannel2();                                                      //включить канал 2 
  }                                                               
}

//счетчики задержки включения каналов 1 и 2
static __IO uint8_t PowerCh1DelayCnt = 0;
static __IO uint8_t PowerCh2DelayCnt = 0;

//включить питание на канале 1
void EnablePowerChannel1()
{
  PowerCh1DelayCnt = POWER_CH1_DELAY_SEC;                                       //установить время задержки включения питания на канале 1
  GPIO_WriteLow(KANAL1_PORT,KANAL1_PIN);
}

//включить питание на канале 2
void EnablePowerChannel2()
{
  
  PowerCh2DelayCnt = POWER_CH2_DELAY_SEC;                                       //установить время задержки включения питания на канале 2
  GPIO_WriteLow(KANAL2_KANAL3_PORT,KANAL2_PIN);
  FreeObjectOnMainPowerOff(&PuData);
  
}


//по прерыванию от таймера задержки включения питания каналов 1,2 (декремент ежесекундно)
void OnPowerChanTimerInterrupt()
{
  if(PowerCh1DelayCnt > 0)
  {
    PowerCh1DelayCnt--;
  }
  else
  {
    GPIO_WriteHigh(KANAL1_PORT,KANAL1_PIN);
  }
  
  if(PowerCh2DelayCnt > 0)
  {
    PowerCh2DelayCnt--;
  }
  else
  {
    GPIO_WriteHigh(KANAL2_KANAL3_PORT,KANAL2_PIN);
  }
}

//индикация бипером неверного положения платформы
static void IndicateUnknownPlatformPosition()
{
  BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);
  GPIO_WriteHigh(HL2_LED_PORT,HL1_HL2_LED_PIN);  
  Delay(800);
  BEEP_Cmd(DISABLE);
  GPIO_WriteLow(HL2_LED_PORT,HL1_HL2_LED_PIN);
  Delay(800);
  BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);
  GPIO_WriteHigh(HL2_LED_PORT,HL1_HL2_LED_PIN);
  Delay(800);
  BEEP_Cmd(DISABLE);
  GPIO_WriteLow(HL2_LED_PORT,HL1_HL2_LED_PIN);
  Delay(800);
  BeepSetToneAndStart(BEEP_FREQUENCY_1KHZ);
  GPIO_WriteHigh(HL2_LED_PORT,HL1_HL2_LED_PIN);
  Delay(800);
  BEEP_Cmd(DISABLE);
  GPIO_WriteLow(HL2_LED_PORT,HL1_HL2_LED_PIN);
}

//вернуть платформу в 0 и освободить объект
static void FreeObjectOnMainPowerOff(PuDataTypeDef *puData)
{
  
  if(puData->PlatformState == PLATFORM_POS_ZERO)                                //если платформа уже в 0 - освободить объект
  {
    SetCommand(&PuData,COMMAND_FREE_OBJECT,CONTROLLER_OPERATOR);    
  }
  else
  {
    SetCommand(&PuData,COMMAND_RETURN0,CONTROLLER_OPERATOR);                    //иначе - вернуть платформу в 0
  }
}







