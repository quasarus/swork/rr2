#include "main.h"
#include "periph_init.h"
#include "adc_handle.h"
#include "motor_control.h"
#include "stm8s.h"


static void StopAdcTimer();
static void StartAdcTimer();
static bool IsAdcValOverrun(uint16_t *values,uint16_t len,double thr);
static bool IsAlreadyObjectHold(PuDataTypeDef *puData);

static uint16_t LlAdcValues[MAX_ADC_MEAS_LL]={0};                                  //буфер измеренных значений АЦП канала датчика света
static uint16_t OverloadM2AdcValues[MAX_ADC_MEAS_OVL]={0};                          //буфер измеренных значений АЦП канала перегрузки мотора
static uint16_t LlAdcMeasNum = 0;                                               //текущее число измерений на канале датчика света
static uint16_t OverloadM2AdcMeasNum = 0;                                       //текущее число измерений на канале перегрузки мотора

//установить канал для измерений
void SetAdcChannel(ADC_PU_Channel channel)
{
  /* Очистить значение канала АЦП */
  ADC2->CSR &= (uint8_t)(~ADC2_CSR_CH);
  /* Установить значение канала АЦП */
  ADC2->CSR |= (uint8_t)(channel);  
}

//по прерыванию от таймера измерения значений АЦП                               
void OnAdcHandleTimer()
{
  StopAdcTimer();                                                               //остановить таймер
  SetAdcChannel(PuData.CurrentAdcConvChan);                                     //установить канал для измерения
  ADC2_Cmd(ENABLE);                                                             //включить АЦП
  ADC2_StartConversion();                                                       //запуск преобразования
}


//по завершению преобразования АЦП
void OnEndOfAdcConversion()
{
  ADC2_Cmd(DISABLE);                                                            //выключить АЦП
  uint16_t adcVal = ADC2_GetConversionValue();                                  //получить значение АЦП
  switch(PuData.CurrentAdcConvChan)                                             //в зависимости от измеряемого канала
  {
    case OVERLOAD_PU_ADC_CHANNEL:                                               //если измерения были на канале перегрузки мотора
    {
      OverloadM2AdcValues[OverloadM2AdcMeasNum] = adcVal;                       //сохранить измеренное значение в буфер
      //инкремент числа измерений
      OverloadM2AdcMeasNum = OverloadM2AdcMeasNum < (MAX_ADC_MEAS_OVL - 1) ? (OverloadM2AdcMeasNum + 1) : 0;      
      if(OverloadM2AdcMeasNum == (MAX_ADC_MEAS_OVL - 1))                            //если заполнен буфер измерений
      {
        if(IsAdcValOverrun(OverloadM2AdcValues,MAX_ADC_MEAS_OVL,OVERLOAD_ADC_TRH) == TRUE)       //если значение АЦП выше порогового
        {
          M2Stop();                                                             //остановить мотор
          switch(PuData.State)                                                  //установить состояние автомата
          {
            case STATE_CATCHING_OBJECT:                                         //если было состояние захвата объекта - установить состояние "объект захвачен"
            {
              PuData.State = STATE_HOLD_OBJECT;
              break;
            }
            case STATE_REALEASING_OBJECT:                                       //если было состояние освобождения объекта - установить состояние "объект освобожден"
            {
              PuData.State = STATE_RELEASED_OBJECT;
              break;
            }
          }          
        }
      }      
      break;
    }
    case LL_PU_ADC_CHANNEL:                                                     //если измерения были на канале измерения освещенности
    {
      LlAdcValues[LlAdcMeasNum] = adcVal;                                       //сохранить измеренное значение в буфер
      //инкремент числа измерений
      LlAdcMeasNum = LlAdcMeasNum < (MAX_ADC_MEAS_LL - 1) ? (LlAdcMeasNum + 1) : 0;
      
      if(LlAdcMeasNum == (MAX_ADC_MEAS_LL - 1))                                    //если заполнен буфер измерений
      {
        if(IsAdcValOverrun(LlAdcValues,MAX_ADC_MEAS_LL,LL_ADC_TRH) == TRUE && IsAlreadyObjectHold(&PuData) == FALSE)        //если превышен порог о объект уже не удерживается/в ходе захвата
        {
          PuData.CurrentAdcConvChan = OVERLOAD_PU_ADC_CHANNEL;                  //установить канал измерения - перегрузки мотора        
          
          SetCommand(&PuData,COMMAND_CATCH_OBJECT,CONTROLLER_OPERATOR);          //перейти к состоянию захвата объекта
        }
      }      
      break;
    }
    default:
      PuData.CurrentAdcConvChan = OVERLOAD_PU_ADC_CHANNEL;                      
      break;
  }
  StartAdcTimer();                                                              //запуск таймера АЦП
}

//проверка превышения измеренного значения АЦП и порогового
static bool IsAdcValOverrun(uint16_t *values,uint16_t len,double thr)
{
  uint32_t sum = 0;
  //усреднение значения в буфере и возврат значения, если порог превышен
  for(uint16_t cnt = 0; cnt < len; cnt++)
  {
    sum += values[cnt];
  }
  double averVal = sum / len;
  return (averVal > thr) ? TRUE : FALSE;  
}

//запуск таймера АЦП
static void StartAdcTimer()
{
  if((TIM2->CR1 & TIM2_CR1_CEN) == 0)                                           //проверка что таймер уже не включен
  {
    StopAdcTimer();                                                             //останов таймера
    //Включить прерывание таймера по обновлению
    TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
    //Пуск таймера
    TIM2_Cmd(ENABLE);
  }
}

//останов таймера
static void StopAdcTimer()
{
  if((TIM2->CR1 & TIM2_CR1_CEN) != 0)                                           //проверка что таймер уже не выключен
  {
    TIM2_ClearFlag(TIM2_FLAG_UPDATE);
    TIM2_ITConfig(TIM2_IT_UPDATE, DISABLE);
    TIM2_Cmd(DISABLE);
  }
}

//проверка, что объект уже не удерживается или не в процессе захвата
static bool IsAlreadyObjectHold(PuDataTypeDef *puData)
{
  uint8_t holdRealeaseStates = (uint8_t)(STATE_START_CATCH_OBJECT | STATE_CATCHING_OBJECT | STATE_START_REALEASE_OBJECT |
                                    STATE_REALEASING_OBJECT | STATE_RELEASED_OBJECT | STATE_HOLD_OBJECT);
  bool isAtObjHoldRealeaseState = ((holdRealeaseStates & ((uint8_t)puData->State)) > 0) ? TRUE : FALSE;
  return (puData->ObjHoldState == OBJ_IS_HOLD) | (isAtObjHoldRealeaseState == TRUE) ? TRUE : FALSE;
}