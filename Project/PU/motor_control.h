#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H


#include "stm8s.h"
#include "pu_work.h"
#include "main.h"

#define M1_TACT_NUM 4                                                           //число тактов мотора вращения платформы
#define M1_SPIN_DELAY_MS 2                                                      //время проворацивания мотора вращения платформы на 1 шаг
#define ADD_ROTATE_NUM 130

void M2StartCatch(void);
void M2StartFree(void);
void M2Stop(void);
void StartPlatformRotation(bool isClockWise);
void ContinueRotatePlatform(PuDataTypeDef *puData);

void M1Stop(void);
void HoldSolenoid(PuDataTypeDef *puData);
void RealiseSolenoid(PuDataTypeDef *puData);
#endif

 