#include "main.h"
#include "stm8s_it.h"
#include "periph_init.h"
#include "stm8s.h"
#include "pu_work.h"
#include "bluetooth_control.h"
#include "string.h"
#include "public.h"

//Буферы UART модуля Bluettoth для связи с МПК
static char Uart1RxBuf[UART_BUF_SIZE]={0};
static char Uart1AnswBuf[UART_BUF_SIZE]={0};

//Буферы UART модуля Bluettoth для связи с смартфоном
static char Uart3RxBuf[UART_BUF_SIZE]={0};
static char Uart3AnswBuf[UART_BUF_SIZE]={0};

static void OnCommandRecognizeUart1(PuDataTypeDef *puData, UserCommandTypeDef userCommand);
static void OnCommandRecognizeUart3(PuDataTypeDef *puData, UserCommandTypeDef userCommand);
//отправка строки по UART на модуль Bluetooth для связи с МПК
uint8_t SendATCommandUart3(PuDataTypeDef *puData,const char *command,const char *answer, bool isWaitAnswer)
{ 
    puData->Uart3BytesRxNum = 0;                                                //обнуление числа принятых байт 
    
    puData->Uart3AnswerLen = strlen(answer);                                    //сохранение ожидаемой длины ответа
    //UART3_ITConfig(UART3_IT_RXNE_OR, ENABLE);                                   //включение прерывания UART3
    
    
    for(uint16_t cnt = 0;cnt < strlen(command);cnt++)                           //отправка символов по UART
    {
      UART3_SendData8(command[cnt]);
      while (UART3_GetFlagStatus(UART3_FLAG_TXE) == RESET);                     //ожидание очистки буфера отправки UART
    } 
    
    if(isWaitAnswer == FALSE)                                                   //если не выставлен флаг необходимости ждать ответ - завершить отправку
    { return 0;}
    
    memcpy(Uart3AnswBuf,answer,puData->Uart3AnswerLen);                         //копирование в буфер строки, ожидаемой в качестве ответа
    
    Delay(BT_ACK_WAIT_MS);                                                      //выдержать таймаут ожидания ответа
    //сравнение приемного буфера и строки, которую ожидалось принять
    uint8_t cmpResult = Buffercmp(Uart3AnswBuf,Uart3RxBuf,puData->Uart3AnswerLen) == 0 ? 0 : 1;
    puData->Uart3BytesRxNum = 0;                                                //обнуление числа принятых байт
    memset(Uart3RxBuf,0,UART_BUF_SIZE);                                         //очистка буфера
    return cmpResult;  
}

//отправка строки по UART на модуль Bluetooth для связи с смартфоном (ф-ция аналогична SendATCommandUart3 см. выше)
uint8_t SendATCommandUart1(PuDataTypeDef *puData,const char *command,const char *answer, bool isWaitAnswer)
{ 
    puData->Uart1BytesRxNum = 0;                                                
    
    puData->Uart1AnswerLen = strlen(answer);
    //UART1_ITConfig(UART1_IT_RXNE_OR, ENABLE);
    
    for(uint16_t cnt = 0;cnt < strlen(command);cnt++)
    {
      UART1_SendData8(command[cnt]);
      while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET);
    } 
    
    if(isWaitAnswer == FALSE)
    { return 0;}
    
    memcpy(Uart1AnswBuf,answer,puData->Uart1AnswerLen);
    
    Delay(BT_ACK_WAIT_MS);
    uint8_t cmpResult = Buffercmp(Uart1AnswBuf,Uart1RxBuf,puData->Uart1AnswerLen) == 0 ? 0 : 1;
    puData->Uart1BytesRxNum = 0;
    memset(Uart1RxBuf,0,UART_BUF_SIZE);
    return cmpResult;  
}



//конфигурирование Bluetooth-модуля для связи с МПК
void ConfigMpkBluetoothModule(PuDataTypeDef *puData)
{
  uint8_t result;  
  result = SendATCommandUart3(puData,"AT","OK",FALSE);                          //отправка команды для проверки связи 
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами
  //result = SendATCommandUart3(puData,"AT+DEFAULT","OK",FALSE);                          //отправка команды для проверки связи 
  //Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами
                                           
  result = SendATCommandUart3(puData,"AT+NAME=?","PU_MPK",TRUE);                //отправка команды-запроса имени устройства Bluetooth
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами
  if(result != 0)                                                               //если имя не совпадает - установить имя
  {
    result = SendATCommandUart3(puData,"AT+NAME=PU_MPK","OKsetNAME",FALSE);
    Delay(BT_MODULE_RESTART_TIME_MS*2);                                         //выдерживание паузы между командами
  }
}

//конфигурирование Bluetooth-модуля для связи с смартфоном
void ConfigSmartphoneBluetoothModule(PuDataTypeDef *puData)
{
  uint8_t result;  
  result = SendATCommandUart1(puData,"AT","OK",FALSE);                          //отправка команды для проверки связи
  
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами
  result = SendATCommandUart1(puData,"AT+NAME=?","PU",TRUE);                    //отправка команды-запроса имени устройства Bluetooth
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами
  if(result != 0)                                                               //если имя не совпадает - установить имя
  {
    result = SendATCommandUart1(puData,"AT+NAME=PU","OKsetNAME",FALSE);
    Delay(BT_MODULE_RESTART_TIME_MS*2);
  }
}

//
//прием символов при передачи команды с смартфона
void OnUart1ByteReceive(PuDataTypeDef *puData)
{
  uint8_t recByte = UART1_ReceiveData8();                                       //чтение принятого символа в приемный буфер
  Uart1RxBuf[puData->Uart1BytesRxNum] = recByte; 
   
  
  if(((char)recByte) == '\r')                                                   //если принят символ завершения команды, проверить команду
  {    
    if(CheckCircularBuffer(BT_COMMAND_PLUS90,Uart1RxBuf,UART_BUF_SIZE,puData->Uart1BytesRxNum) == 0)                           
    {
      OnCommandRecognizeUart1(puData,COMMAND_PLUS90DEG);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_MINUS90,Uart1RxBuf,UART_BUF_SIZE,puData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(puData,COMMAND_MINUS90DEG);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_ZERO,Uart1RxBuf,UART_BUF_SIZE,puData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(puData,COMMAND_RETURN0);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_CATCH,Uart1RxBuf,UART_BUF_SIZE,puData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(puData,COMMAND_CATCH_OBJECT);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_FREE,Uart1RxBuf,UART_BUF_SIZE,puData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(puData,COMMAND_FREE_OBJECT);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_IS_PU_REQ_NO_CR,Uart1RxBuf,UART_BUF_SIZE,puData->Uart1BytesRxNum) == 0)
    {        
      SendAnswerAsPuSlaveForSmartphone(BT_COMMAND_IS_PU_ANSWER_NO);       
    }    
  }
  
  //инкремент числа принятых символов
  puData->Uart1BytesRxNum = (puData->Uart1BytesRxNum < (UART_BUF_SIZE - 1)) ? puData->Uart1BytesRxNum + 1 : 0;  
  
  if(puData->Uart1BytesRxNum >= puData->Uart1AnswerLen)
  {    
    TimingDelay = 0;
  }
  UART1_ClearITPendingBit(UART1_IT_RXNE);  
}




//прием символов при передачи команды с МПК
void OnUart3ByteReceive(PuDataTypeDef *puData)
{                    
  uint8_t recByte = UART3_ReceiveData8();                                       //чтение принятого символа в приемный буфер
  Uart3RxBuf[puData->Uart3BytesRxNum] = recByte; 
   
  
  if(((char)recByte) == '\r')
  {    
    if(CheckCircularBuffer(BT_COMMAND_PLUS90,Uart3RxBuf,UART_BUF_SIZE,puData->Uart3BytesRxNum) == 0)                           //выполнение проверки, что принята команда поворота +90
    {
      OnCommandRecognizeUart3(puData,COMMAND_PLUS90DEG);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_MINUS90,Uart3RxBuf,UART_BUF_SIZE,puData->Uart3BytesRxNum) == 0)
    {
      OnCommandRecognizeUart3(puData,COMMAND_MINUS90DEG);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_ZERO,Uart3RxBuf,UART_BUF_SIZE,puData->Uart3BytesRxNum) == 0)
    {
      OnCommandRecognizeUart3(puData,COMMAND_RETURN0);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_CATCH,Uart3RxBuf,UART_BUF_SIZE,puData->Uart3BytesRxNum) == 0)
    {
      OnCommandRecognizeUart3(puData,COMMAND_CATCH_OBJECT);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_FREE,Uart3RxBuf,UART_BUF_SIZE,puData->Uart3BytesRxNum) == 0)
    {
      OnCommandRecognizeUart3(puData,COMMAND_FREE_OBJECT);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_IS_PU_REQ_NO_CR,Uart3RxBuf,UART_BUF_SIZE,puData->Uart3BytesRxNum) == 0)
    {        
      SendAnswerAsPuSlaveForMpk(BT_COMMAND_IS_PU_ANSWER_YES);       
    }    
  }
  
  //инкремент числа принятых символов
  puData->Uart3BytesRxNum = (puData->Uart3BytesRxNum < (UART_BUF_SIZE - 1)) ? puData->Uart3BytesRxNum + 1 : 0;  
  
  if(puData->Uart3BytesRxNum >= puData->Uart3AnswerLen)
  {    
    TimingDelay = 0;
  }
  UART3_ClearITPendingBit(UART3_IT_RXNE);
    
   
}

//при распознавании команды с МПК
static void OnCommandRecognizeUart3(PuDataTypeDef *puData, UserCommandTypeDef userCommand)
{                                        
  SetCommand(puData,userCommand,CONTROLLER_MPK);                                //установка команды 
  //SendAnswerAsPuSlaveForMpk(BT_RESPONSE_OK);  
}

//при распознавании команды с смартфона
static void OnCommandRecognizeUart1(PuDataTypeDef *puData, UserCommandTypeDef userCommand)
{              
                                         
  SetCommand(puData,userCommand,CONTROLLER_SMARTPHONE);                         //установка команды
  //SendAnswerAsPuSlaveForSmartphone(BT_RESPONSE_OK);                             //отправка подтверждения приема команды                        
  
}



//отправить подтверждение для МПК
void SendAnswerAsPuSlaveForMpk(const char* answer)
{
  for(uint8_t cnt = 0; cnt < strlen(answer);cnt++)
  {
    UART3_SendData8(answer[cnt]);
    while (UART3_GetFlagStatus(UART3_FLAG_TXE) == RESET);
  }
}

//отправить подтверждение для смартфона
void SendAnswerAsPuSlaveForSmartphone(const char* answer)
{
  for(uint8_t cnt = 0; cnt < strlen(answer);cnt++)
  {
    UART1_SendData8(answer[cnt]);
    while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET);
  }
}



