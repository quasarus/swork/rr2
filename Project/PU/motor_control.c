#include "main.h"
#include "stm8s_it.h"
#include "periph_init.h"
#include "stm8s.h"
#include "pu_work.h"
#include "bluetooth_control.h"
#include "string.h"
#include "motor_control.h"

//значения для выставления на портЮ управляющий мотором поворота платформы
const uint8_t masksM1CW[]={0x03,0x06,0x0C,0x09};                                //для вращения по часовой стрелке
const uint8_t masksM1CCW[]={0x09,0x0C,0x06,0x03};                               //для вращения против часовой стрелки

static void AdditioanalRotate(PuDataTypeDef *puData,uint32_t stepNum,uint8_t currentTact);
//включение мотора для захвата объекта
void M2StartCatch(void)
{
  GPIO_WriteHigh(M2I_A_PORT,M2I_A_PIN);
  GPIO_WriteLow(M2I_B_PORT,M2I_B_PIN);
}

//включение мотора для освобождения объекта
void M2StartFree(void)
{
  GPIO_WriteLow(M2I_A_PORT,M2I_A_PIN);
  GPIO_WriteHigh(M2I_B_PORT,M2I_B_PIN);
}


//продолжить вращение платформы
void ContinueRotatePlatform(PuDataTypeDef *puData)
{
  puData->CurrentAdcConvChan = OVERLOAD_PU_ADC_CHANNEL;                         //установка текущего канала измерения АЦП - измерение перегрузки
  GPIO_WriteHigh(ENM1_PORT,ENM1_PIN);                                           //включение драйвера мотора
  HoldSolenoid(puData);                                                         //отжатие соленойда
  for(uint8_t cnt=0;cnt < M1_TACT_NUM; cnt++)                                   //вращение мотора
  {
     uint8_t portState = GPIOB->ODR;                                            //вычитка состояния порта, на котором находится драйвер двигателя
     //использование маски для состояния порта в зависимости от направления вращения
     uint8_t mask = (puData->IsClockwiseRotate == TRUE) ? masksM1CCW[cnt] : masksM1CW[cnt];
     portState &= 0xF0;                                                         
     portState |= mask;                                                         //запись по маске
     GPIOB->ODR = portState;                                                    //запись состояния порта
     Delay(M1_SPIN_DELAY_MS);                                                                  //задержка
     if(puData->PlatformState == puData->EndPlatformState)                      //когда платформа дошла до конечной позиции
     {
       AdditioanalRotate(puData,ADD_ROTATE_NUM,cnt);
       RealiseSolenoid(puData);                                                 //отпускание соленойда
       GPIO_WriteLow(ENM1_PORT,ENM1_PIN);                                       //выключение драйвера двигателя
       //если платформа в позиции 0 и питания на основном канале нет - отпустить объект
       SetState(&PuData,STATE_IDLE);
       if((puData->PlatformState == PLATFORM_POS_ZERO) && (CheckStatState() == TRUE))
       {
         SetCommand(&PuData,COMMAND_FREE_OBJECT,CONTROLLER_OPERATOR);         
       }       
       
       puData->CurrentAdcConvChan = LL_PU_ADC_CHANNEL;                          //переключить канал измерения АЦП на измерения уровня света
       break;
     }
  }
}

//отжатие соленойда
void HoldSolenoid(PuDataTypeDef *puData)
{
  if((TIM1->CR1 & TIM1_CR1_CEN) == 0)                                           //если таймер ШИМ для соленойда еще не запущен
  {
    
    GPIO_WriteHigh(KANAL2_KANAL3_PORT,KANAL3_PIN);                              //резко отжать соленоид
    
    for(uint32_t cnt=0;cnt<200000;cnt++){}                                       //ожидание  
    //ШИМить соленойд для удержания
    TIM1_CtrlPWMOutputs(ENABLE);                                                
    TIM1_Cmd(ENABLE);                                                           
  }  
}

//отпустить соленоид
void RealiseSolenoid(PuDataTypeDef *puData)
{ 
  
  TIM1_CtrlPWMOutputs(DISABLE);
  TIM1_Cmd(DISABLE);
  GPIO_WriteLow(KANAL2_KANAL3_PORT,KANAL3_PIN);  
}

//остановить мотор захвата объекта
void M2Stop(void)
{
  GPIO_WriteLow(M2I_A_PORT,M2I_A_PIN);
  GPIO_WriteLow(M2I_B_PORT,M2I_B_PIN);
}

static void AdditioanalRotate(PuDataTypeDef *puData,uint32_t stepNum,uint8_t currentTact)
{
  uint8_t tact = currentTact;
  
  for(uint32_t cnt = 0; cnt < stepNum;cnt++)
  {
    tact = tact < (M1_TACT_NUM - 1) ? (tact + 1) : 0;
    uint8_t portState = GPIOB->ODR;                                            //вычитка состояния порта, на котором находится драйвер двигателя
     //использование маски для состояния порта в зависимости от направления вращения
    uint8_t mask = (puData->IsClockwiseRotate == TRUE) ? masksM1CCW[tact] : masksM1CW[tact];
    portState &= 0xF0;                                                         
    portState |= mask;                                                         //запись по маске
    GPIOB->ODR = portState;                                                    //запись состояния порта
    Delay(M1_SPIN_DELAY_MS); 
  }  
}



