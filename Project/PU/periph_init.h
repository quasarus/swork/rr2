#ifndef PERIPH_INIT_H
#define PERIPH_INIT_H

void ClockInit(void);
void GpioInit(void);
void UART1_Config(void);
void UART3_Config(void);
void BeeperInit(void);
void ADC_Config(void);
void TIM4_Config(void);
void TIM2_Config(void);
void TIM1_Config(void);
void TIM3_Config(void);

#define DEFAULT_UART_SPEED 9600                                                 //скорость UART


//периоды таймеров 2,3,4
#define TIM4_PERIOD 124
#define TIM2_PERIOD 1561
#define TIM3_PERIOD 15624


//переопределения портов и пинов
#define PLUS_MINUS90DEG_PORT GPIOD
#define PLUS_90DEG_PIN GPIO_PIN_0
#define MINUS_90DEG_PIN GPIO_PIN_2
#define PLUS_MINUS90DEG_PINS (PLUS_90DEG_PIN | MINUS_90DEG_PIN)

#define STAT_PORT GPIOC
#define STAT_PIN GPIO_PIN_2

#define KEY1H_PORT GPIOD
#define KEY2H_PORT GPIOD
#define KEY1H_PIN GPIO_PIN_3
#define KEY2H_PIN GPIO_PIN_7


#define LED1H_PORT GPIOA
#define LED1H_PIN GPIO_PIN_6

#define LED2H_PORT GPIOC
#define LED2H_PIN GPIO_PIN_1

#define HL1_LED_PORT GPIOE
#define HL2_LED_PORT GPIOB
#define HL1_HL2_LED_PIN GPIO_PIN_5

#define BL1ON_BL2ON_PORT GPIOC
#define BL1ON_PIN GPIO_PIN_6
#define BL2ON_PIN GPIO_PIN_7

#define POS1_POS2_POS3_PORT GPIOE
#define POS1_PIN GPIO_PIN_0
#define POS2_PIN GPIO_PIN_1
#define POS3_PIN GPIO_PIN_2
#define POS_PINS (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2)

#define ENM1_PORT GPIOG
#define ENM1_PIN GPIO_PIN_0

#define NAM1_PORT GPIOB
#define _1AM_PIN GPIO_PIN_0
#define _2AM_PIN GPIO_PIN_1
#define _3AM_PIN GPIO_PIN_2
#define _4AM_PIN GPIO_PIN_3
#define NAM1_PINS (_1AM_PIN | _2AM_PIN | _3AM_PIN | _4AM_PIN)

#define M2I_A_PORT GPIOG
#define M2I_A_PIN GPIO_PIN_1

#define M2I_B_PORT GPIOB
#define M2I_B_PIN GPIO_PIN_4

#define KANAL1_PORT GPIOE
#define KANAL2_KANAL3_PORT GPIOC
#define KANAL1_PIN GPIO_PIN_6
#define KANAL2_PIN GPIO_PIN_5
#define KANAL3_PIN GPIO_PIN_3
#define KANAL2_KANAL3_PINS (KANAL2_PIN | KANAL3_PIN)

#define OVERLOAD_LL_PORT GPIOB
#define OVERLOAD_LL_PINS (GPIO_PIN_6 | GPIO_PIN_7)

void Exti_Init(void);
void Delay(__IO uint32_t nTime);
void TimingDelay_Decrement(void);

extern __IO uint32_t TimingDelay;
#endif

