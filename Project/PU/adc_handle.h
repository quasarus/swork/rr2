#ifndef ADC_HANDLE_H
#define ADC_HANDLE_H
#include "stm8s.h"

typedef enum 
{
  OVERLOAD_PU_ADC_CHANNEL = ADC2_CHANNEL_6,                                     //канал перегрузки мотора захвата объекта
  LL_PU_ADC_CHANNEL = ADC2_CHANNEL_7                                            //канал измерения уровня света
}ADC_PU_Channel;                                                                //тип канала АЦП

#define MAX_ADC_MEAS_LL 20                                                      //число измерений АЦП для усреднения
#define MAX_ADC_MEAS_OVL 15                                                      //число измерений АЦП для усреднения

#define LL_ADC_TRH 50                                                           //порог АЦП для канала измерения уровня света
#define OVERLOAD_ADC_TRH 50                                                    //порог АЦП для канала измерения перегрузки мотора

void SetAdcChannel(ADC_PU_Channel channel);
void OnAdcHandleTimer();
void OnEndOfAdcConversion();

#endif

