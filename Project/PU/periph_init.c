#include "main.h"
#include "periph_init.h"
#include "stm8s.h"
#include "stm8s_tim5.h"
__IO uint32_t TimingDelay = 0;





void ClockInit(void)
{
  CLK_DeInit();                                                 //сброс установок тактирования
  
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);                      //установка предделителя МК =1
  
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);                      //установка предделителя внутр. источника частоты =1
  
  //установить источником тактовой - внутренний генератор, выключить прерывание по смене частоты, 
  //выключить текущий источник тактовой частоты
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSI, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);
}

void GpioInit(void)
{
  
  //инициализация пинов светодиодов индикации - выход push-pull,10Мгц, низкий уровень
  GPIO_Init(HL1_LED_PORT, HL1_HL2_LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(HL2_LED_PORT, HL1_HL2_LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  
  //выставление флага AFR7 в рег-ре OPT2 для того, чтобы альтернативная ф-ция порта PD4 была - выход для beeper
  //OPT->OPT2 |= BEEPER_AFR7_MASK;
  
  //инициализация пинов светодиодов индикации модулей Bluetooth - вход без подтяжки и внешнего прерывания
  GPIO_Init(LED1H_PORT, LED1H_PIN, GPIO_MODE_IN_FL_NO_IT);
  GPIO_Init(LED2H_PORT, LED2H_PIN, GPIO_MODE_IN_FL_NO_IT);
  
  //инициализация пинов очистки памяти модулей Bluetooth - выход (push-pull) - низкий уровень, 10Мгц
  GPIO_Init(KEY1H_PORT, KEY1H_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(KEY2H_PORT, KEY2H_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  
  //инициализация пина датчика наличия напряжения +12В Основное - вход без подтяжки с внешним прерыванием
  GPIO_Init(STAT_PORT, STAT_PIN, GPIO_MODE_IN_FL_IT);
  
  //инициализация пинов включения модулей Bluetooth - выход push-pull,10Мгц, низкий уровень
  GPIO_Init(BL1ON_BL2ON_PORT, (GPIO_Pin_TypeDef)( BL1ON_PIN | BL2ON_PIN), GPIO_MODE_OUT_PP_LOW_FAST);
  
  //инициализация пинов тумблера +-90гр. - вход без подтяжки и с внешним прерыванием
  GPIO_Init(PLUS_MINUS90DEG_PORT, (GPIO_Pin_TypeDef)(PLUS_MINUS90DEG_PINS), GPIO_MODE_IN_FL_IT);
  
  //инициализация пинов датчиков положения - вход без подтяжки и с внешним прерыванием
  GPIO_Init(POS1_POS2_POS3_PORT, (GPIO_Pin_TypeDef)(POS_PINS), GPIO_MODE_IN_FL_IT);
  
  //инициализация пина включения драйвера управления мотором 1, выход push-pull,низкий уров., 10Мгц
  GPIO_Init(ENM1_PORT, ENM1_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  
  //инициализация пинов управления мотором 1, выход push-pull,низкий уров., 10Мгц
  GPIO_Init(NAM1_PORT, (GPIO_Pin_TypeDef)(NAM1_PINS), GPIO_MODE_OUT_PP_LOW_FAST);
  
  //инициализация пинов управления мотором 2, выход push-pull,низкий уров., 10Мгц
  GPIO_Init(M2I_A_PORT, M2I_A_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(M2I_B_PORT, M2I_B_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  
  //инициализация пинов управления реле и соленойдом каналов 1,2,3 - выход push-pull,низкий уров., 10Мгц
  GPIO_Init(KANAL1_PORT, KANAL1_PIN, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(KANAL2_KANAL3_PORT, KANAL2_PIN, GPIO_MODE_OUT_PP_HIGH_FAST);
  GPIO_Init(KANAL2_KANAL3_PORT, KANAL3_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  //GPIO_WriteLow(KANAL2_KANAL3_PORT,KANAL3_PIN);
  /*инициализация пинов АЦП - вход без подтяжки и прерывания
  OVERLOAD - вход6 АЦП
  LL - вход7 АЦП
  */
  GPIO_Init(OVERLOAD_LL_PORT,(GPIO_Pin_TypeDef)(OVERLOAD_LL_PINS), GPIO_MODE_IN_FL_NO_IT);
  
  //установка пина бипера в 0
  GPIO_Init(GPIOD, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST);
  
  GPIO_WriteHigh(HL1_LED_PORT,HL1_HL2_LED_PIN);
    
  //включить модули bluetooth (Low - включение)
  GPIO_WriteLow(BL1ON_BL2ON_PORT,BL1ON_PIN);
  GPIO_WriteLow(BL1ON_BL2ON_PORT,BL2ON_PIN);
  
  //инициализация пина бипера
  InitBeeperPin();
  
}

void UART1_Config(void)
{
  UART1_DeInit();                                       //сброс UART1
  
  //инициализация UART1 - pin SLK выключен, 8-1 передача без контроля четности, TX RX включены
  UART1_Init((uint32_t)(DEFAULT_UART_SPEED), UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO,
              UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_ITConfig(UART1_IT_RXNE_OR, ENABLE);
}

void UART3_Config(void)
{
  UART3_DeInit();                                       //сброс UART3
  
  //инициализация UART3 - 8-1 передача без контроля четности, TX RX включены
  UART3_Init((uint32_t)(DEFAULT_UART_SPEED), UART3_WORDLENGTH_8D, UART3_STOPBITS_1, UART3_PARITY_NO,
              UART3_MODE_TXRX_ENABLE);
  UART3_ITConfig(UART3_IT_RXNE_OR, ENABLE);                                   //включение прерывания UART3
}


void BeeperInit(void)
{
  //сброс установок бипера
  BEEP_DeInit();                                         
  //установка по умолчанию самой высокой частоты тона из 3х доступных
  BEEP_Init(BEEP_FREQUENCY_4KHZ);
  
}

void ADC_Config(void)
{
  ADC2_DeInit();                                //сброс АЦП2
  
  /*инициализация АЦП - канал 6, единичное преобразование, частота выборки - половина тактовой МКб
  преобразование от внутреннего события таймера, правое выравнивание, внешний триггер выключен, триггер Шмитта выкл. */
  ADC2_Init(ADC2_CONVERSIONMODE_SINGLE, ADC2_CHANNEL_10, ADC2_PRESSEL_FCPU_D2, \
            ADC2_EXTTRIG_TIM, DISABLE, ADC2_ALIGN_RIGHT, ADC2_SCHMITTTRIG_CHANNEL6,\
            DISABLE);
  
  ADC2_ITConfig(ENABLE);
  
}

void TIM4_Config(void)
{
  /*Частота тактирования таймеров до предделителя равна 16Мгц, при установке предделителя таймера, с 
  коэффициентом 128, счетная частота таймера становится 125000Гц, для временной базы таймера,
  равной 1мс, необходимо значение периода таймера, равное 124. Время=(1/125000Гц)*(1+124)*/
  
  
  /* Time base configuration */
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
  
  //Очистка флага обновления таймера
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
  //Включить прерывание таймера по обновлению
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  //Пуск таймера
  TIM4_Cmd(ENABLE);
  
}

//Инициализация вспомогательного таймера для выполнения измерений АЦП
void TIM2_Config(void)
{
  /*Частота тактирования таймеров до предделителя равна 16Мгц, при установке предделителя таймера, с 
  коэффициентом 512, счетная частота таймера становится 31250Гц, для временной базы таймера,
  равной ~50мс, необходимо значение периода таймера, равное 1561. Время=(1/31250Гц)*(1+1561)*/
  
  TIM2_TimeBaseInit(TIM2_PRESCALER_512,TIM2_PERIOD);
  
  //Очистка флага обновления таймера
  TIM2_ClearFlag(TIM2_FLAG_UPDATE);
  //Включить прерывание таймера по обновлению
  TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
  
  
}

//Инициализация вспомогательного таймера для переключения реле каналов 1,2
void TIM3_Config(void)
{
  /*Частота тактирования таймеров до предделителя равна 16Мгц, при установке предделителя таймера, с 
  коэффициентом 1024, счетная частота таймера становится ~15625Гц, для временной базы таймера,
  равной 1с, необходимо значение периода таймера, равное 15625. Время=(1/15625)*(1+15624)*/
  
  TIM3_TimeBaseInit(TIM3_PRESCALER_1024,TIM3_PERIOD);
  
  //Очистка флага обновления таймера
  TIM3_ClearFlag(TIM3_FLAG_UPDATE);
  TIM3_ITConfig(TIM3_IT_UPDATE, ENABLE);
  TIM3_Cmd(ENABLE);
}




//Инициализация вспомогательного таймера ШИМ соленоида
void TIM1_Config(void)
{ 
  
  
  TIM1_DeInit();
                
     TIM1_TimeBaseInit(16, TIM1_COUNTERMODE_UP, 200, TIM1_OPMODE_SINGLE);
                
     TIM1_OC3Init(TIM1_OCMODE_PWM1, 
                  TIM1_OUTPUTSTATE_ENABLE, 
                  TIM1_OUTPUTNSTATE_DISABLE, 
                  100, 
                  TIM1_OCPOLARITY_HIGH, 
                  TIM1_OCNPOLARITY_LOW, 
                  TIM1_OCIDLESTATE_RESET, 
                  TIM1_OCNIDLESTATE_RESET);
                
    TIM1_CtrlPWMOutputs(DISABLE);
    //TIM1_Cmd(DISABLE);
}

//настройка внешних прерываний
void Exti_Init(void)
{
  
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOA, EXTI_SENSITIVITY_FALL_ONLY);       //настройка прерывания порта А по спадающему фронту    
  
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOC, EXTI_SENSITIVITY_RISE_ONLY);       //настройка прерываний порта C по возрастающему фронту
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_RISE_FALL);       //настройка прерываний порта D по возрастающему и спадающему фронту
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_RISE_ONLY);       //настройка прерываний порта E по возрастающему фронту
  
  ITC_SetSoftwarePriority(ITC_IRQ_TIM4_OVF,ITC_PRIORITYLEVEL_0);                //выставление приоритета прерывания счетного таймера миллисекундной задержки в максимум
  
  //выставление приоритетов прерывания UART и остальных таймеров в минимум
  ITC_SetSoftwarePriority(ITC_IRQ_UART3_TX,ITC_PRIORITYLEVEL_2);                  
  ITC_SetSoftwarePriority(ITC_IRQ_UART3_RX,ITC_PRIORITYLEVEL_2);
  ITC_SetSoftwarePriority(ITC_IRQ_UART1_TX,ITC_PRIORITYLEVEL_2);
  ITC_SetSoftwarePriority(ITC_IRQ_UART1_RX,ITC_PRIORITYLEVEL_2);
  
  ITC_SetSoftwarePriority(ITC_IRQ_TIM1_OVF,ITC_PRIORITYLEVEL_2);
  ITC_SetSoftwarePriority(ITC_IRQ_TIM2_OVF,ITC_PRIORITYLEVEL_2);
  ITC_SetSoftwarePriority(ITC_IRQ_TIM3_OVF,ITC_PRIORITYLEVEL_2);
  
  //выставление приоритетов внешних прерываний в среднее значение
  ITC_SetSoftwarePriority(ITC_IRQ_PORTA,ITC_PRIORITYLEVEL_1);
  ITC_SetSoftwarePriority(ITC_IRQ_PORTC,ITC_PRIORITYLEVEL_1);
  ITC_SetSoftwarePriority(ITC_IRQ_PORTD,ITC_PRIORITYLEVEL_1);
  ITC_SetSoftwarePriority(ITC_IRQ_PORTE,ITC_PRIORITYLEVEL_1);
  ITC_SetSoftwarePriority(ITC_IRQ_ADC2,ITC_PRIORITYLEVEL_1); 
  
  
  //EXTI_SetTLISensitivity(EXTI_TLISENSITIVITY_RISE_ONLY);
}

//декремент счетчика миллисекундной задержки
void TimingDelay_Decrement(void)
{
  TimingDelay = TimingDelay > 0 ? (TimingDelay - 1) : TimingDelay; 
  
}

//миллисекундная задержка
void Delay(__IO uint32_t nTime)
{
  TimingDelay = nTime;
  

  //Ждать пока значение задержки не достигло 0
  while (TimingDelay != 0);  
  
}
            
            