#ifndef BLUETOOTH_CONTROL_H
#define BLUETOOTH_CONTROL_H


#include "stm8s.h"
#include "pu_work.h"
#include "main.h"

#define BT_ACK_WAIT_MS 2000                                                     //таймаут ожидания ответа по UART
#define UART_BUF_SIZE 16                                                        //размер буферов UART

uint8_t SendATCommandUart3(PuDataTypeDef *puData,const char *command,const char *answer, bool isWaitAnswer);
uint8_t SendATCommandUart1(PuDataTypeDef *puData,const char *command,const char *answer, bool isWaitAnswer);
void ConfigSmartphoneBluetoothModule(PuDataTypeDef *puData);
void ConfigMpkBluetoothModule(PuDataTypeDef *puData);
void SendAnswerAsPuSlaveForMpk(const char* answer);
void SendAnswerAsPuSlaveForSmartphone(const char* answer);


extern char Uart1RxBuf[UART_BUF_SIZE];
extern char Uart3RxBuf[UART_BUF_SIZE];
#endif

 