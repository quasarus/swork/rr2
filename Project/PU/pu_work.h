#ifndef MPK_WORK_H
#define MPK_WORK_H


#include "stm8s.h"
#include "pu_work.h"

void MainWork(PuDataTypeDef *puData);
void SetCommand(PuDataTypeDef *puData,UserCommandTypeDef command,PuControllerTypeDef controller);
void OnPosSensorInterrupt();
void OnToggleSwitchInterrupt();
void OnPowerChanTimerInterrupt();
bool GetPlatformPosition(PuDataTypeDef *puData);
void GetOperatorToggleSwitchState(PuDataTypeDef *puData,uint8_t portState);
void OnExtPortDInterrupt();

void EnablePowerChannel1();
void EnablePowerChannel2();

bool CheckStatState();

void OnStatInterrupt();


void OnDebounceTimerInterrupt();

#define SENSOR_POS90_PIN_MASK 0x01                                              //маска состояния пина датчика положения +90
#define SENSOR_MINUS90_PIN_MASK 0x02                                            //маска состояния пина датчика положения -90
#define SENSOR_ZERO_PIN_MASK 0x04                                               //маска состояния пина датчика положения 0

#define POS_SENSORS_GPIO_MASK (SENSOR_ZERO_PIN_MASK | SENSOR_MINUS90_PIN_MASK | SENSOR_POS90_PIN_MASK)

#define POWER_CH1_DELAY_SEC 45                                                  //задержка подачи питания на канал 1
#define POWER_CH2_DELAY_SEC 25                                                  //задержка подачи питания на канал 2
#endif

 