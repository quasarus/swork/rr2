#ifndef __MAIN_H
#define __MAIN_H
#include "stm8s.h"
#include "public.h"
#include "adc_handle.h"

//#define USE_BEEPER                                                            //включение/выключение кода для работы бипера


typedef enum
{
  CONTROLLER_NONE,                                                              //нет
  CONTROLLER_MPK,                                                               //МПК
  CONTROLLER_SMARTPHONE,                                                        //Смартфон
  CONTROLLER_OPERATOR                                                           //Оператор
}PuControllerTypeDef;                                                           //тип оператора, задающего команду ПУ

typedef enum
{
  OBJ_HOLD_UNKNOWN,                                                             //неизвестно
  OBJ_IS_HOLD,                                                                  //удержание
  OBJ_IS_RELEASED                                                               //отпускание
}PuObjHoldStateTypeDef;                                                         //тип операции с захватом объекта

typedef enum
{
  PLATFORM_POS_UNKNOWN,                                                         //неизвестно
  PLATFORM_POS_PLUS90DEG,                                                       //+90
  PLATFORM_POS_MINUS90DEG,                                                      //-90
  PLATFORM_POS_ZERO                                                             //0
}PuPlatformStateTypeDef;                                                        //тип позиции платформы

typedef enum
{  
  STATE_IDLE,                                                                   //бездействие
  STATE_START_CATCH_OBJECT,                                                     //старт захвата объекта
  STATE_CATCHING_OBJECT,                                                        //процесс захвата объекта
  STATE_START_REALEASE_OBJECT,                                                  //старт освобождения объекта
  STATE_REALEASING_OBJECT,                                                      //процесс освобождения объекта
  STATE_RELEASED_OBJECT,                                                        //после освобождения объекта
  STATE_HOLD_OBJECT,                                                            //после захвата объекта
  STATE_EXEC_NEW_COMMAND,                                                       //выполнение новой команды
  STATE_CHECK_CH2_POWER,                                                        //проверка питания канала 2
  STATE_SEND_ANSWER_PU_YES,                                                     //отправка ответа, что модуль не для связи с МПК
  STATE_SEND_ANSWER_PU_NO,                                                      //отправка ответа, что модуль для связи с МПК
  STATE_ROTATE_PLATFORM                                                         //состояние вращения платформы
}PuStateTypeDef;                                                                //тип состояний автомата состояний ПУ


  
typedef struct
{
  PuStateTypeDef State;                                                         //текущее состояние автомата ПУ
  PuStateTypeDef PrevState;                                                     //предыдущее состояние автомата ПУ  
  uint16_t Uart1BytesRxNum;                                                     //число принятых байт по UART1
  uint16_t Uart3BytesRxNum;                                                     //число принятых байт по UART3 
  uint16_t Uart1AnswerLen;                                                      //длина ответа на запрос или команду по UART1
  uint16_t Uart3AnswerLen;                                                      //длина ответа на запрос или команду по UART3
  UserCommandTypeDef UserCommand;                                               //команда пользователя
  UserCommandTypeDef OnBusyUserCommand;
  PuControllerTypeDef Controller;                                               //управляющий ПУ
  PuObjHoldStateTypeDef ObjHoldState;                                           //состояние удержания объекта
  PuPlatformStateTypeDef PlatformState;                                         //положение платформы
  PuPlatformStateTypeDef EndPlatformState;                                      //конечное состояние платформы
  bool IsClockwiseRotate;                                                       //TRUE - вращение платформы должно быть по часовой стрелке, FALSE - против
  ADC_PU_Channel CurrentAdcConvChan;                                            //текущий канал измерения АЦП  
}PuDataTypeDef;                                                                 //тип данных ПУ




void SetState(PuDataTypeDef *puData,PuStateTypeDef newState);
void GoToPreviousState(PuDataTypeDef *puData);
extern PuDataTypeDef PuData;




#endif

