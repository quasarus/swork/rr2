#ifndef BLUETOOTH_CONTROL_H
#define BLUETOOTH_CONTROL_H


#include "stm8s.h"
#include "mpk_work.h"
#include "main.h"
#include "public.h"

#define UART_BUF_SIZE 16                                                        //размер буферов UART



#define BT_MODULE_WAIT_CONNECT_TIME 1500                                        //время ожидания высокого уровня на пине KEY модуля Bluettoth



extern __IO uint32_t PuBtModuleConnectCnt;
extern __IO uint32_t SmartBtModuleConnectCnt;

uint8_t SendATCommandUart3(MpkDataTypeDef *mpkData,const char *command,const char *answer, bool isWaitAnswer);
uint8_t SendATCommandUart1(MpkDataTypeDef *mpkData,const char *command,const char *answer, bool isWaitAnswer);
void ConfigPuBluetoothModule(MpkDataTypeDef *mpkData);
void ConfigSmartphoneBluetoothModule(MpkDataTypeDef *mpkData);
void SetCommand(MpkDataTypeDef *mpkData,UserCommandTypeDef command);
void ClearMemBluetoothPu(void);
void SendAnswerAsMpkSlave(const char* answer);


#endif

 