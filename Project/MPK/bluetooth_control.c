#include "main.h"
#include "stm8s_it.h"
#include "periph_init.h"
#include "stm8s.h"
#include "mpk_work.h"
#include "bluetooth_control.h"
#include "string.h"
#include "public.h"

/*счетчики, определяющие таймаут последнего события среза фронта на выводе LED модуля BT (если в течение этого времени не было спада фронта, считать, что модуль BT
сопряжен
*/
__IO uint32_t PuBtModuleConnectCnt = BT_MODULE_WAIT_CONNECT_TIME;
__IO uint32_t SmartBtModuleConnectCnt = BT_MODULE_WAIT_CONNECT_TIME;

static void OnCommandRecognizeUart1(MpkDataTypeDef *mpkData, UserCommandTypeDef userCommand);
//Буферы UART модуля Bluettoth для связи с ПУ
static char Uart1RxBuf[UART_BUF_SIZE]={0};
static char Uart1AnswBuf[UART_BUF_SIZE]={0};

//Буферы UART модуля Bluettoth для связи с смартфоном
static char Uart3RxBuf[UART_BUF_SIZE]={0};
static char Uart3AnswBuf[UART_BUF_SIZE]={0};

static void SendAnswerAsMpkSlave(const char* answer);

//отправка строки по UART на модуль Bluetooth для связи с ПУ
uint8_t SendATCommandUart3(MpkDataTypeDef *mpkData,const char *command,const char *answer, bool isWaitAnswer)
{ 
    mpkData->Uart3BytesRxNum = 0;                                               //обнуление числа принятых байт                               
    
    mpkData->Uart3AnswerLen = strlen(answer);                                   //сохранение ожидаемой длины ответа
    //UART3_ITConfig(UART3_IT_RXNE_OR, ENABLE);                                   //включение прерывания UART3
    memset(Uart3RxBuf,0,UART_BUF_SIZE);
    
    for(uint16_t cnt = 0;cnt < strlen(command);cnt++)                           //отправка символов по UART
    {
      UART3_SendData8(command[cnt]);
      while (UART3_GetFlagStatus(UART3_FLAG_TXE) == RESET);                     //ожидание очистки буфера отправки UART
    } 
    
    if(isWaitAnswer == FALSE)                                                   //если не выставлен флаг необходимости ждать ответ - завершить отправку
    { return 0;}
    
    
    memcpy(Uart3AnswBuf,answer,mpkData->Uart3AnswerLen);                        //копирование в буфер строки, ожидаемой в качестве ответа
    
    Delay(BT_ACK_WAIT_MS);                                                      //выдержать таймаут ожидания ответа
    //сравнение приемного буфера и строки, которую ожидалось принять
    uint8_t cmpResult = Buffercmp(Uart3AnswBuf,Uart3RxBuf,mpkData->Uart3AnswerLen) == 0 ? 0 : 1;
    mpkData->Uart3BytesRxNum = 0;                                               //обнуление числа принятых байт
    
    return cmpResult;  
}

//отправка строки по UART на модуль Bluetooth для связи с смартфоном (ф-ция аналогична SendATCommandUart3 см. выше)
uint8_t SendATCommandUart1(MpkDataTypeDef *mpkData,const char *command,const char *answer, bool isWaitAnswer)
{ 
    mpkData->Uart1BytesRxNum = 0;
   
    mpkData->Uart1AnswerLen = strlen(answer);
    
    
    for(uint16_t cnt = 0;cnt < strlen(command);cnt++)
    {
      UART1_SendData8(command[cnt]);
      while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET);
    } 
    
    if(isWaitAnswer == FALSE)
    {
      
      return 0;      
    }
    
    memcpy(Uart1AnswBuf,answer,mpkData->Uart1AnswerLen);
    
    Delay(BT_ACK_WAIT_MS);    
    uint8_t cmpResult = Buffercmp(Uart1AnswBuf,Uart1RxBuf,mpkData->Uart1AnswerLen) == 0 ? 0 : 1;
    mpkData->Uart1BytesRxNum = 0;
    memset(Uart1RxBuf,0,UART_BUF_SIZE);
    return cmpResult;  
}


//конфигурирование Bluetooth-модуля для связи с ПУ
void ConfigPuBluetoothModule(MpkDataTypeDef *mpkData)
{
  uint8_t result;
  
  result = SendATCommandUart3(&MpkData,"AT","OK",FALSE);                        //отправка команды для проверки связи  
    
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами

  
  result = SendATCommandUart3(&MpkData,"AT+ROLE=?","Master",TRUE);              //отправка команды - запроса роли платы, ожидание строки, подтверждающей, что модуль в роли мастера
  
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами 
  
  if(result != 0)
  {    
    result = SendATCommandUart3(&MpkData,"AT+ROLE=M","OK",TRUE);                //если роль - не мастер, то отправить команду перевода модуля в режим мастера
    Delay(BT_MODULE_RESTART_TIME_MS*2);                                         //выдерживание паузы между командами
  }    
  if(result != 0)
  {
    return;
  }
  PuBtModuleConnectCnt = BT_MODULE_WAIT_CONNECT_TIME;                           //установка таймаута офидания фронта для пина KEY модуля Bluetooth                          
}

//конфигурирование Bluetooth-модуля для связи с смартфоном
void ConfigSmartphoneBluetoothModule(MpkDataTypeDef *mpkData)
{
  uint8_t result;  
  result = SendATCommandUart1(&MpkData,"AT","OK",FALSE);                        //отправка команды для проверки связи
  
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами
  
  result = SendATCommandUart1(&MpkData,"AT+NAME=?","MPK",TRUE);                 //проверка имени Bluetooth устройства
  
  Delay(BT_MODULE_RESTART_TIME_MS*2);                                           //выдерживание паузы между командами
  
  if(result != 0)                                                               //если имя не совпадает с ожидаемым - установить имя
  {
    result = SendATCommandUart1(&MpkData,"AT+NAME=MPK","OKsetNAME",FALSE);
    Delay(BT_MODULE_RESTART_TIME_MS*2);
  } 
  
}

//установка и запись в EEPROM команды для ПУ
void SetCommand(MpkDataTypeDef *mpkData,UserCommandTypeDef command)
{
  TimingDelay = 0;                                                              //сброс счетчика задержки
  mpkData->PrevUserCommand = mpkData->UserCommand;                              //запоминание предыдущей команды
  WritePuCommandToFlash(command);                                               //запись команды для ПУ в энергонезависимую память
  mpkData->UserCommand = command;                                               //запоминание команды в структуре данных ПУ
}

//прием символов при передачи команды с смартфона
void OnUart1ByteReceive(MpkDataTypeDef *mpkData)
{   
  
  uint8_t recByte = UART1_ReceiveData8();                                       //чтение принятого символа в приемный буфер
  Uart1RxBuf[mpkData->Uart1BytesRxNum] = recByte; 
   
  
  if(((char)recByte) == '\r')                                                   //если принят символ завершения команды, проверить команду
  {    
    if(CheckCircularBuffer(BT_COMMAND_PLUS90,Uart1RxBuf,UART_BUF_SIZE,mpkData->Uart1BytesRxNum) == 0)                           
    {
      OnCommandRecognizeUart1(mpkData,COMMAND_PLUS90DEG);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_MINUS90,Uart1RxBuf,UART_BUF_SIZE,mpkData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(mpkData,COMMAND_MINUS90DEG);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_ZERO,Uart1RxBuf,UART_BUF_SIZE,mpkData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(mpkData,COMMAND_RETURN0);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_CATCH,Uart1RxBuf,UART_BUF_SIZE,mpkData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(mpkData,COMMAND_CATCH_OBJECT);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_FREE,Uart1RxBuf,UART_BUF_SIZE,mpkData->Uart1BytesRxNum) == 0)
    {
      OnCommandRecognizeUart1(mpkData,COMMAND_FREE_OBJECT);    
    }
    else if(CheckCircularBuffer(BT_COMMAND_IS_PU_REQ_NO_CR,Uart1RxBuf,UART_BUF_SIZE,mpkData->Uart1BytesRxNum) == 0)
    {        
      SendAnswerAsMpkSlave(BT_COMMAND_IS_PU_ANSWER_NO);       
    }    
  }
  
  //инкремент числа принятых символов
  mpkData->Uart1BytesRxNum = (mpkData->Uart1BytesRxNum < (UART_BUF_SIZE - 1)) ? mpkData->Uart1BytesRxNum + 1 : 0;  
  
  if(mpkData->Uart1BytesRxNum >= mpkData->Uart1AnswerLen)
  {    
    TimingDelay = 0;
  }
  UART1_ClearITPendingBit(UART1_IT_RXNE);                                                                //очистка счетчика миллисекундной задержки задержки
  
}

//при распознавании команды с смартфона
static void OnCommandRecognizeUart1(MpkDataTypeDef *mpkData, UserCommandTypeDef userCommand)
{                                  
                                         
  SetCommand(mpkData,userCommand);                                              //установка команды   
                         
  
}

//прием символов при ожидании подтверждения с ПУ 
void OnUart3ByteReceive(MpkDataTypeDef *mpkData)
{ 
  //UART3_ITConfig(UART3_IT_RXNE_OR, DISABLE);
  Uart3RxBuf[mpkData->Uart3BytesRxNum] = UART3_ReceiveData8();
                                         //очистка бита обслуживания
  //инкремент числа принятых символов
  mpkData->Uart3BytesRxNum = (mpkData->Uart3BytesRxNum < UART_BUF_SIZE) ? mpkData->Uart3BytesRxNum + 1 : 0;
  if(mpkData->Uart3BytesRxNum == 0)                                             //если буфер переполнился, выполнить его очистку
  {
    memset(Uart3RxBuf,0,UART_BUF_SIZE);
    mpkData->Uart3BytesRxNum = 0;
  }  
  
  if(mpkData->Uart3BytesRxNum < mpkData->Uart3AnswerLen)
  {
    UART3_ClearITPendingBit(UART3_IT_RXNE);
    return;
  }
  UART3_ClearITPendingBit(UART3_IT_RXNE);
  TimingDelay = 0;
    
}


//по событию спадания фронта на пине LED модуля связи с ПУ (если произошло событие спада фронта, считать, что модуль не подключился ни к одному из клиентов)
void PuBtModuleLedPinFalling(void)
{
  
  PuBtModuleConnectCnt = BT_MODULE_WAIT_CONNECT_TIME;                           //обновить значение счетчика таймаута                           
  MpkData.IsBluetoothPuConnected = FALSE;                                       //сбросить флаг наличия соединения
}

//по событию спадания фронта на пине LED модуля связи с смартфоном (если произошло событие спада фронта, считать, что модуль не подключился ни к одному из клиентов)
void SmartBtModuleLedPinFalling(void)
{
  SmartBtModuleConnectCnt = BT_MODULE_WAIT_CONNECT_TIME;                        //обновить значение счетчика таймаута
  MpkData.IsBluetoothSmartConnected = FALSE;                                    //сбросить флаг наличия соединения
}

//декремент значения счетчиков  таймаута последнего события среза фронта на выводе LED модулей BT  
void ConnectCounters_Decrement(void)
{
  
  if(PuBtModuleConnectCnt > 0)
  {
    PuBtModuleConnectCnt--;
  }
  else
  {
    MpkData.IsBluetoothPuConnected = TRUE;                                      //установка флага наличия соединения   
  }
  
  if(SmartBtModuleConnectCnt > 0)
  {
    SmartBtModuleConnectCnt--;
  }
  else
  {
    MpkData.IsBluetoothSmartConnected = TRUE;                                   //установка флага наличия соединения
  }  
}

//очистка памяти, хранящей MAC клиента, с которым было соединения в модуле связи с ПУ (выполняется разрыв соединения с клиентом)
void ClearMemBluetoothPu(void)
{
  for(uint16_t cnt=0; cnt < 5; cnt++)
  {
    GPIO_WriteHigh(KEY1H_PORT,KEY1H_PIN);
    Delay(150);
    GPIO_WriteLow(KEY1H_PORT,KEY1H_PIN);
    Delay(150);
  }  
}

//отправка ответа на смартфон
static void SendAnswerAsMpkSlave(const char* answer)
{  
  for(uint8_t cnt = 0; cnt < strlen(answer);cnt++)
  {
    UART1_SendData8(answer[cnt]);
    while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET);
  }  
}
