#include "main.h"
#include "periph_init.h"
#include "stm8s.h"
#include "mpk_work.h"
#include "bluetooth_control.h"
#include "public.h"
#include "stm8s_beep.h"

static bool CheckMpkPuBtConnection(MpkDataTypeDef *mpkData);
static void SendBtCommandToPu(MpkDataTypeDef *mpkData,UserCommandTypeDef command);
static void SetState(MpkDataTypeDef *mpkData,MpkStateTypeDef newState);

static void RequestToPu(MpkDataTypeDef *mpkData);
static void ClearMemAndResetBtModule(MpkDataTypeDef *mpkData);

static void StartTimer3();
static void StopTimer3();

//цикл работы программы
void WorkLoop(MpkDataTypeDef *mpkData)
{
  
  switch(mpkData->State)                                                        //автомат состояний
  { 
    case STATE_IDLE:                                                            //бездействие
    {
      break;
    }
    case STATE_CONNECT_TO_PU:                                                   //соединение по каналу связи с ПУ (может соединиться не с ПУ)
    {
      StartTimer3();                                                            //запуск индикации процедуры соединения с ПУ  
      ClearMemAndResetBtModule(mpkData);                                        //очистка памяти модуля Bluetooth ПУ
      RequestToPu(mpkData);                                                     //запрос к подключенному устройству, является ли оно ПУ          
      break;
    }
    case STATE_CONNECTED_TO_PU:                                                 //по окончанию соединения именно с ПУ
    {
      StopTimer3();                                                             //останов индикации процедуры соединения с ПУ                                                         
      if(CheckMpkPuBtConnection(mpkData) == TRUE)                               //проверка соединения с ПУ
      {
        if(GPIO_ReadInputPin(STAT_PORT,STAT_PIN) == RESET)                      //по подаче питания на канал 2 перейти в другое состояние
        { 
          SetState(mpkData,STATE_POWER_CH2_ON);          
        }
      }  
      else
      {
#ifdef USE_BEEPER
        BeepSetToneAndStart(BEEP_FREQUENCY_4KHZ);
        Delay(500);
        BEEP_Cmd(DISABLE);
#endif
        SetState(mpkData,STATE_CONNECT_TO_PU);
      }
      break;
    }
    case STATE_POWER_CH2_ON:                                                    //при подаче питания на канал 2
    {
      if(CheckMpkPuBtConnection(mpkData) == TRUE)                               //проверить связь с ПУ
      {
        //mpkData->BeforePowerCh2OnCommand = mpkData->UserCommand;                //запомнить команду, которая была перед подачей питания на канал 2
        SendBtCommandToPu(mpkData,mpkData->UserCommand);                        //отправка команды на ПУ
        SetState(mpkData,STATE_WAIT_CH2_OFF);                                   //перейти в состояние ожидания выключения питания на канале 2      
      }
      else
      {
#ifdef USE_BEEPER
        BeepSetToneAndStart(BEEP_FREQUENCY_4KHZ);
        Delay(500);
        BEEP_Cmd(DISABLE);
#endif
        SetState(mpkData,STATE_CONNECT_TO_PU);                                  //если связь пропала - перейти в режим соединения с ПУ
      }
      break;
    }    
    case STATE_WAIT_CH2_OFF:                                                    //при отключении питания на канале 2
    {
      
      if(CheckMpkPuBtConnection(mpkData) == TRUE)                               //проверить связь с ПУ
      {
        if((uint8_t)GPIO_ReadInputPin(STAT_PORT,STAT_PIN) > 0)                  //если пропало питание на канале 2
        {
          SendBtCommandToPu(mpkData,mpkData->BeforePowerCh2OnCommand);          //отправить команду на ПУ для возврата в то положение, что было до подачи питания на канал 2
          SetState(mpkData,STATE_CONNECTED_TO_PU);
        }                
      }
      else
      {
#ifdef USE_BEEPER
        BeepSetToneAndStart(BEEP_FREQUENCY_4KHZ);
        Delay(500);
        BEEP_Cmd(DISABLE);
#endif
        SetState(mpkData,STATE_CONNECT_TO_PU);                                  //если связь пропала - перейти в режим соединения с ПУ
      }
      break;
    }
    case STATE_FAULT_RESET_PAIR:                                                //состояние сброса сопряжения
    {       
      ClearMemBluetoothPu();                                                    //очистка памяти модуля
      GPIO_WriteLow(HL2_LED_PORT,HL2_LED_PIN);                                  //выключение светодиода индикации
      
#ifdef USE_BEEPER                                                               //при использовании бипера - звуковой сигнал, иначе просто задержка
      BeepSetToneAndStart(BEEP_FREQUENCY_4KHZ);                                 
      Delay(300);
      BEEP_Cmd(DISABLE);
#else
      Delay(300);
#endif 
      //выключение обоих модулей Bluetooth
      GPIO_WriteHigh(BL1ON_BL2ON_PORT,BL1ON_PIN);                               
      GPIO_WriteHigh(BL1ON_BL2ON_PORT,BL2ON_PIN);
      Delay(300);                                                               //задержка
      GPIO_WriteLow(BL1ON_BL2ON_PORT,BL2ON_PIN);                                //включение модуля связи с смартфоном      
      
      for(uint16_t cnt = 0;cnt < 10;cnt++)                                      //если идет удержание кнопки...
      {
        /*если кнопка перестала удерживаться раньше заданного времени - короткое нажатие,
        то перейти в состояние бездействия*/
        if(GPIO_ReadInputPin(FAULT_BUTTON_PORT, FAULT_BUTTON_PIN) == SET)
        {
          SetState(mpkData,STATE_IDLE);                                                 
          return;
        }
        Delay(100);
      }     
      
      GPIO_WriteLow(BL1ON_BL2ON_PORT,BL1ON_PIN);                                //включение модуля связи с ПУ
#ifdef USE_BEEPER                                                               //озвучивание состояния начала соединения с ПУ
      BeepSetToneAndStart(BEEP_FREQUENCY_4KHZ);
      Delay(300);
      BEEP_Cmd(DISABLE);
      Delay(300);
      BeepSetToneAndStart(BEEP_FREQUENCY_2KHZ);
      Delay(300);
      BEEP_Cmd(DISABLE);
#else
      Delay(600);
#endif
      SetState(mpkData,STATE_CONNECT_TO_PU);                                    //переход в состояние соединения с ПУ     
      break;
    }
        
  }

}

//очистка и сброс модуля связи с ПУ
static void ClearMemAndResetBtModule(MpkDataTypeDef *mpkData)
{
  ClearMemBluetoothPu();                                                        //очистка памяти модуля
  
  if(mpkData->IsBluetoothPuConnected == FALSE)                                  //проверка, что модуль отключился
  {

    SendATCommandUart3(mpkData,"AT+RESET","OK",FALSE);                          //отправка команды сброса модуля
    
    Delay(500);                                                                 //ожидание готовности модуля
    SendATCommandUart3(&MpkData,"AT","OK",FALSE);                               //отправка команды запроса статуса
    Delay(BT_MODULE_RESTART_TIME_MS*2);                                         //ожидание выполнения команды
  
  }  
  PuBtModuleConnectCnt = BT_MODULE_WAIT_CONNECT_TIME;
  Delay(4000);
}

//запрос к подключенному к каналу связи с ПУ устройству, является ли оно ПУ
static void RequestToPu(MpkDataTypeDef *mpkData)
{
  if(MpkData.IsBluetoothPuConnected == TRUE)                                    //если есть соединение с каким-то устройством по каналу связи с ПУ
  {
    //отправка команда запроса, является ли устройство ПУ
    uint8_t result;        
    result = SendATCommandUart3(mpkData,BT_COMMAND_IS_PU_REQ,BT_COMMAND_IS_PU_ANSWER_YES,TRUE);
    if(result != 0)                                                             //если таймаут ответа или неверный ответ
    {
      ClearMemBluetoothPu();                                                    //очистка модуля Bluetooth
      Delay(1500);
      result = SendATCommandUart3(mpkData,BT_COMMAND_RESET,"OK",TRUE);          //отправка команды сброса модуля
      GPIO_WriteLow(HL2_LED_PORT,HL2_LED_PIN);                                  //погасить пин индикации 
    }
    else                                                                        //... считать, что с ПУ произощло соединение
    { 
#ifdef USE_BEEPER  
   BeepSetToneAndStart(BEEP_FREQUENCY_2KHZ);
   Delay(300);
   BEEP_Cmd(DISABLE);
   Delay(300);
   BeepSetToneAndStart(BEEP_FREQUENCY_2KHZ);
   Delay(300);
   BEEP_Cmd(DISABLE);
#endif    
      SetState(mpkData,STATE_CONNECTED_TO_PU);                                  //установить состояние соединения с ПУ      
      GPIO_WriteHigh(HL2_LED_PORT,HL2_LED_PIN);                                 //зажечь светодиод индикации
    }        
   }
   else                                                                         //...если потеряна связь с ПУ
   {
     SetState(mpkData,STATE_CONNECT_TO_PU);
   }
   
}

//установка состояния автомата
static void SetState(MpkDataTypeDef *mpkData,MpkStateTypeDef newState)
{
  mpkData->PrevState = mpkData->State;                                          //запомнить предыдущее состояние                                          
  mpkData->State = newState;                                                    //записать состояние в структуру данных МПК
}

//отправка AT-команды на ПУ
static void SendBtCommandToPu(MpkDataTypeDef *mpkData,UserCommandTypeDef command)
{
  
  switch(command)
  {
    case COMMAND_PLUS90DEG:
    {
      SendATCommandUart3(mpkData,BT_COMMAND_PLUS90_CR,"OK",FALSE);
      break;
    }
    case COMMAND_MINUS90DEG:
    {
      SendATCommandUart3(mpkData,BT_COMMAND_MINUS90_CR,"OK",FALSE);
      break;
    }
    case COMMAND_RETURN0:
    {
      SendATCommandUart3(mpkData,BT_COMMAND_ZERO_CR,"OK",FALSE);
      break;
    }
    default:
      break;
  }
  
  
}

//проверка соединения с устройством на канале ПУ
static bool CheckMpkPuBtConnection(MpkDataTypeDef *mpkData)
{
  if(mpkData->IsBluetoothPuConnected == FALSE)
  {
    mpkData->State = STATE_CONNECT_TO_PU;
    GPIO_WriteLow(HL2_LED_PORT,HL2_LED_PIN);
    return FALSE;
  }
  GPIO_WriteHigh(HL2_LED_PORT,HL2_LED_PIN);        
  return TRUE;
}

//при нажатии аварийной кнопки
void OnFaultButtonPress()
{ 
  MpkData.State = STATE_FAULT_RESET_PAIR;
}

//по нажатию кнопки команды
void OnCommandCodeButtonsPress()
{
  TIM2_Cmd(DISABLE);                                                            //выключение таймера
  TIM2_ITConfig(TIM2_IT_UPDATE, DISABLE);                                       //выкл. прерываний таймера
  
  TIM2_ClearFlag(TIM2_FLAG_UPDATE);                                             //Очистка флага обновления таймера
  
  TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);                                        //Включить прерывание таймера по обновлению
  
  TIM2_Cmd(ENABLE);                                                             //Пуск таймера 
  
}

//по обновлению счетчика таймера дребезга кнопок команды
void OnButtonTimerUpdate(void)
{
  TIM2_ClearFlag(TIM2_FLAG_UPDATE);                                             //Очистка флага обновления таймера
  TIM2_ITConfig(TIM2_IT_UPDATE, DISABLE);                                       //выкл. прерываний таймера
  
  TIM2_Cmd(DISABLE);                                                            //выключение таймера
  
  //чтение состояния кнопок команды
  uint8_t codeButtonsState = GPIO_ReadInputData(KK_BUTTONS_PORT) & ((uint8_t)(KK_PINS));
  //запись команды в зависимости от состояния кнопок
  switch(codeButtonsState)
  {
    case KK_CODE_PLUS90:
    {      
      SetCommand(&MpkData,COMMAND_PLUS90DEG);
      BeepShort(BEEP_FREQUENCY_4KHZ);
      break;
    }
    case KK_CODE_MINUS90:
    {
      SetCommand(&MpkData,COMMAND_MINUS90DEG);      
      BeepShort(BEEP_FREQUENCY_4KHZ);
      break;
    }
    case KK_CODE_ZERO:
    {
      SetCommand(&MpkData,COMMAND_RETURN0);      
      BeepShort(BEEP_FREQUENCY_4KHZ);
      break;
    }
    default:
      break;
  }
}

static void StartTimer3()
{
  //если таймер 3 еще не включен
  if((TIM3->CR1 & TIM3_CR1_CEN) == 0)
  {
    StopTimer3();  
    //Включить прерывание таймера по обновлению
    TIM3_ITConfig(TIM3_IT_UPDATE, ENABLE);
    //Пуск таймера
    TIM3_Cmd(ENABLE);
  }
  
}

static void StopTimer3()
{
  //если таймер 3 еще не выключен
  if((TIM3->CR1 & TIM3_CR1_CEN) != 0)
  {
    TIM3_ClearFlag(TIM3_FLAG_UPDATE);
    TIM3_ITConfig(TIM3_IT_UPDATE, DISABLE);
    TIM3_Cmd(DISABLE);
  }  
}

//по обновлению таймера индикации
void OnTimer3Update()
{
  GPIO_WriteReverse(HL2_LED_PORT,HL2_LED_PIN);                                  //поменять состояние пина
}

//короткий писк бипером с установкой тона
void BeepShort(BEEP_Frequency_TypeDef tone)
{
  BEEP_Init(tone);
  BEEP_Cmd(ENABLE);  
  for(uint16_t cnt = 0;cnt < BEEP_SHORT_DELAY_CYCLES_NUM;cnt++){}
  BEEP_Cmd(DISABLE);                
}







