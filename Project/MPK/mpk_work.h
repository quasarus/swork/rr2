#ifndef MPK_WORK_H
#define MPK_WORK_H


#include "stm8s.h"
#include "mpk_work.h"


#define KK_CODE_PLUS90 0x06                                                     //состояние кнопок команды, соответствующее положению платформы +90
#define KK_CODE_MINUS90 0x05                                                    //состояние кнопок команды, соответствующее положению платформы -90
#define KK_CODE_ZERO 0x03                                                       //состояние кнопок команды, соответствующее положению платформы 0
#define BEEP_SHORT_DELAY_CYCLES_NUM 13000                                       //число пустых циклов для короткого писка бипера

#define FLASH_COMMAND_ADDRESS 0x4000                                            //адрес в области EEPROM, по которому хранится команда МПК

void WorkLoop(MpkDataTypeDef *mpkData);
void OnFaultButtonPress();
void OnCommandCodeButtonsPress();
void OnButtonTimerUpdate();
void OnTimer3Update();
void BeepShort(BEEP_Frequency_TypeDef tone);

#endif

 