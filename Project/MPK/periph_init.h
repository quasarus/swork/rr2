#ifndef PERIPH_INIT_H
#define PERIPH_INIT_H


#include "stm8s.h"
#include "main.h"
#include "public.h"

void ClockInit(void);
void GpioInit(void);
void BeeperInit(void);
void UART1_Config(void);
void UART3_Config(void);
void Timer4_Init(void);
void Timer2_Init(void);
void Timer3_Init(void);
void Exti_Init(void);

void Delay(__IO uint32_t nTime);

void MpkDataInit(MpkDataTypeDef *mpkData);
void WritePuCommandToFlash(UserCommandTypeDef command);
UserCommandTypeDef ReadPuCommandFromFlash();

extern __IO uint32_t TimingDelay;



#define TIM4_PERIOD 124                                                         //период таймера 4
#define TIM2_PERIOD 3124                                                        //период таймера 2

#define FAULT_BUTTON_PORT GPIOD                                                 //порт аварийной кнопки
#define FAULT_BUTTON_PIN GPIO_PIN_0                                             //пин аварийной кнопки

#define KK_BUTTONS_PORT GPIOE                                                   //порт кнопок команд
//пины кнопок команд
#define KK0_BUTTON_PIN GPIO_PIN_0
#define KK1_BUTTON_PIN GPIO_PIN_1
#define KK2_BUTTON_PIN GPIO_PIN_2
#define KK_PINS (KK0_BUTTON_PIN | KK1_BUTTON_PIN | KK2_BUTTON_PIN)

#define HL1_LED_PORT GPIOE                                                      //порт светодиода 1
#define HL1_LED_PIN GPIO_PIN_5                                                  //пин светодиода 1

#define HL2_LED_PORT GPIOB                                                      //порт светодиода 2
#define HL2_LED_PIN GPIO_PIN_5                                                  //пин светодиода 2

#define LED1H_PORT GPIOA                                                        //порт вывода LED модуля Bluetooth для связи с ПУ
#define LED1H_PIN GPIO_PIN_6                                                    //пин вывода LED модуля Bluetooth для связи с ПУ

#define LED2H_PORT GPIOC                                                        //порт вывода LED модуля Bluetooth для связи с смартфоном
#define LED2H_PIN GPIO_PIN_1                                                    //пин вывода LED модуля Bluetooth для связи с смартфоном
  

#define KEY1H_PORT GPIOD                                                        //порт вывода сброса памяти модуля Bluetooth для связи с ПУ
#define KEY1H_PIN GPIO_PIN_2                                                    //пин вывода сброса памяти модуля Bluetooth для связи с ПУ

#define KEY2H_PORT GPIOC                                                        //порт вывода сброса памяти модуля Bluetooth для связи с смартфоном
#define KEY2H_PIN GPIO_PIN_2                                                    //пин вывода сброса памяти модуля Bluetooth для связи с смартфоном

#define STAT_PORT GPIOD                                                         //порт вывода для контроля питания канала 2
#define STAT_PIN GPIO_PIN_3                                                     //пин вывода для контроля питания канала 2

#define BL1ON_BL2ON_PORT GPIOC                                                  //порт пинов включения модулей Bluetooth
#define BL1ON_PIN GPIO_PIN_6                                                    //пин включения модуля Bluetooth для связи с ПУ
#define BL2ON_PIN GPIO_PIN_7                                                    //пин включения модуля Bluetooth для связи с смартфоном

#define DEFAULT_UART_SPEED 9600                                                 //скорость UART


#endif

 