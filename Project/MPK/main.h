#ifndef __MAIN_H
#define __MAIN_H

#include "stm8s.h"
#include "public.h"

//#define USE_BEEPER                                                            //использовать ли бипер          



typedef enum
{  
  STATE_IDLE,                                                                   //бездействие                                                      
  STATE_CONNECT_TO_PU,                                                          //процесс подключения к ПУ
  STATE_CONNECTED_TO_PU,                                                        //по завершению соединения с ПУ
  STATE_POWER_CH2_ON,                                                           //по включению питания на канале 2
  STATE_WAIT_CH2_OFF,                                                           //по выключению питания на канале 2
  STATE_FAULT_RESET_PAIR,                                                       //сброс сопряжения с устройством на канале связи с ПУ  
}MpkStateTypeDef;                                                               //состояния для автомата состояний МПК


typedef struct
{
  MpkStateTypeDef State;                                                        //текущее состояние автомата
  MpkStateTypeDef PrevState;                                                    //предыдущее состояние автомата
  UserCommandTypeDef UserCommand;                                               //текущая установленная пользователем команда
  UserCommandTypeDef PrevUserCommand;                                           //предыдущая установленная пользователем команда
  UserCommandTypeDef BeforePowerCh2OnCommand;                                   //команда перед подачей на канал 2 питания
  uint16_t Uart1BytesRxNum;                                                     //число принятых байт по UART1
  uint16_t Uart3BytesRxNum;                                                     //число принятых байт по UART3
  uint16_t Uart1AnswerLen;                                                      //длина в символах ожидаемого ответа по UART1
  uint16_t Uart3AnswerLen;                                                      //длина в символах ожидаемого ответа по UART3
  bool IsBluetoothSmartConnected;                                               //есть ли соединение с устройством по каналу связи с смартфоном
  bool IsBluetoothPuConnected;                                                  //есть ли соединение с устройством по каналу связи с ПУ  
}MpkDataTypeDef;                                                                //определение данных МПК

extern MpkDataTypeDef MpkData;



#endif

