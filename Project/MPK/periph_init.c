#include "main.h"
#include "periph_init.h"
#include "bluetooth_control.h"
#include "stm8s.h"
#include "stm8s_flash.h"

void TimingDelay_Decrement(void);
__IO uint32_t TimingDelay = 0;                                  //счетчик миллисекундной задержки



void Delay(__IO uint32_t nTime);

static const UserCommandTypeDef CorrectCommands[]={COMMAND_PLUS90DEG,COMMAND_MINUS90DEG,COMMAND_RETURN0};
static void GetFlashStoredCommand(MpkDataTypeDef *mpkData);
void ClockInit(void)
{  
  
  CLK_DeInit();                                                 //сброс установок тактирования
  
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);                      //установка предделителя МК =1
  
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);                      //установка предделителя внутр. источника частоты =1
  
  //установить источником тактовой - внутренний генератор, выключить прерывание по смене частоты, 
  //выключить текущий источник тактовой частоты
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSI, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);
}

void GpioInit(void)
{
  //инициализация пина аварийной кнопки - вход без подтяжки и с  внешним прерыванием
  GPIO_Init(FAULT_BUTTON_PORT, FAULT_BUTTON_PIN, GPIO_MODE_IN_FL_IT);
  
  //инициализация пинов кода команды - вход без подтяжки и с  внешним прерыванием
  GPIO_Init(KK_BUTTONS_PORT, (GPIO_Pin_TypeDef)(KK_PINS), GPIO_MODE_IN_FL_IT);  
  
  //инициализация пинов светодиодов индикации - выход push-pull,10Мгц, низкий уровень
  GPIO_Init(HL1_LED_PORT, HL1_LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(HL2_LED_PORT, HL2_LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  
  InitBeeperPin();
  
  
  //инициализация пинов светодиодов индикации модулей Bluetooth - вход без подтяжки и с внешним прерыванием
  GPIO_Init(LED1H_PORT, LED1H_PIN, GPIO_MODE_IN_FL_IT);
  GPIO_Init(LED2H_PORT, LED2H_PIN, GPIO_MODE_IN_FL_IT);

  //инициализация пинов очистки памяти модулей Bluetooth - выход (push-pull) - низкий уровень, 10Мгц
  GPIO_Init(KEY1H_PORT, KEY1H_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(KEY2H_PORT, KEY2H_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  
  //инициализация пина датчика наличия напряжения Канала 2 - вход без подтяжки и внешнего прерывания
  GPIO_Init(STAT_PORT, STAT_PIN, GPIO_MODE_IN_FL_NO_IT);
  
  //инициализация пинов включения модулей Bluetooth - выход push-pull,10Мгц, низкий уровень
  GPIO_Init(BL1ON_BL2ON_PORT, (GPIO_Pin_TypeDef)( BL1ON_PIN | BL2ON_PIN), GPIO_MODE_OUT_PP_LOW_FAST);
    
  
  //установка неиспользуемых пинов как вход с подтяжкой к +
  GPIO_Init(GPIOD, GPIO_PIN_7, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(GPIOE, GPIO_PIN_6, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(GPIOC, (GPIO_Pin_TypeDef)(GPIO_PIN_3 | GPIO_PIN_5), GPIO_MODE_IN_PU_NO_IT);  
  GPIO_Init(GPIOG, (GPIO_Pin_TypeDef)(GPIO_PIN_0 | GPIO_PIN_1), GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(GPIOB, (GPIO_Pin_TypeDef)(GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_6 | GPIO_PIN_7), GPIO_MODE_IN_PU_NO_IT);
  
  GPIO_WriteHigh(HL1_LED_PORT,HL1_LED_PIN);                                     //инициализация пина светодиода 1 и его зажигание
  
  //включить модули bluetooth (Low - включение)
  GPIO_WriteLow(BL1ON_BL2ON_PORT,BL1ON_PIN);
  GPIO_WriteLow(BL1ON_BL2ON_PORT,BL2ON_PIN);  
  
  GPIO_Init(GPIOD, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST);
  
  GPIO_Init(GPIOD, GPIO_PIN_5, GPIO_MODE_OUT_OD_HIZ_FAST);                      //инициализация неиспользуемого пина
  
  InitBeeperPin();
}

void UART1_Config(void)
{
  
  UART1_DeInit();                                       //сброс UART1
  
  //инициализация UART1 - pin SLK выключен, 8-1 передача без контроля четности, TX RX включены
  UART1_Init((uint32_t)(DEFAULT_UART_SPEED), UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO,
              UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
  UART1_ITConfig(UART1_IT_RXNE_OR, ENABLE);
}

void UART3_Config(void)
{
  
  UART3_DeInit();                                       //сброс UART3
  
  //инициализация UART3 - 8-1 передача без контроля четности, TX RX включены
  UART3_Init((uint32_t)(DEFAULT_UART_SPEED), UART3_WORDLENGTH_8D, UART3_STOPBITS_1, UART3_PARITY_NO,
              UART3_MODE_TXRX_ENABLE);
  UART3_ITConfig(UART3_IT_RXNE_OR, ENABLE);                                   //включение прерывания UART3
}

void BeeperInit(void)
{
  //сброс установок бипера
  BEEP_DeInit();                                         
  //установка по умолчанию самой высокой частоты тона из 3х доступных
  BEEP_Init(BEEP_FREQUENCY_4KHZ);                       
  
}

void Timer4_Init(void)
{
  /*Частота тактирования таймеров до предделителя равна 16Мгц, при установке предделителя таймера, с 
  коэффициентом 128, счетная частота таймера становится 125000Гц, для временной базы таймера,
  равной 1мс, необходимо значение периода таймера, равное 124. Время=(1/125000Гц)*(1+124)*/
  
  
  /* Time base configuration */
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, TIM4_PERIOD);
  
  //Очистка флага обновления таймера
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
  //Включить прерывание таймера по обновлению
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  //Пуск таймера
  TIM4_Cmd(ENABLE);
    
}

//Инициализация вспомогательного таймера для исключения дребезга кнопок на 100мс
void Timer2_Init(void)
{
  /*Частота тактирования таймеров до предделителя равна 16Мгц, при установке предделителя таймера, с 
  коэффициентом 512, счетная частота таймера становится 31250Гц, для временной базы таймера,
  равной 100мс, необходимо значение периода таймера, равное 3124. Время=(1/31250Гц)*(1+3124)*/
  
  TIM2_TimeBaseInit(TIM2_PRESCALER_512,TIM2_PERIOD*2);
  
  //Очистка флага обновления таймера
  TIM2_ClearFlag(TIM2_FLAG_UPDATE);
  //Включить прерывание таймера по обновлению
  //TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
  
}

//Инициализация вспомогательного таймера для индикации сведодиода HL2 на 300мс
void Timer3_Init(void)
{
  /*Частота тактирования таймеров до предделителя равна 16Мгц, при установке предделителя таймера, с 
  коэффициентом 512, счетная частота таймера становится 31250Гц, для временной базы таймера,
  равной 100мс, необходимо значение периода таймера, равное 3124. Время=(1/31250Гц)*((1+3124)*3)*/
  
  TIM3_TimeBaseInit(TIM3_PRESCALER_512,TIM2_PERIOD*3);
  
  //Очистка флага обновления таймера
  TIM3_ClearFlag(TIM3_FLAG_UPDATE);
  //Включить прерывание таймера по обновлению
  //TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
  
}

void Exti_Init(void)
{
  //включение внешнего прерывания для портов A,C,D,E по спадающему фронту
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOA, EXTI_SENSITIVITY_FALL_ONLY);
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOC, EXTI_SENSITIVITY_FALL_ONLY); 
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_FALL_ONLY);
  EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_FALL_ONLY);
  
  ITC_SetSoftwarePriority(ITC_IRQ_TIM4_OVF,ITC_PRIORITYLEVEL_1);
  
  
  ITC_SetSoftwarePriority(ITC_IRQ_UART3_TX,ITC_PRIORITYLEVEL_2);                  
  ITC_SetSoftwarePriority(ITC_IRQ_UART3_RX,ITC_PRIORITYLEVEL_2);
  
  ITC_SetSoftwarePriority(ITC_IRQ_UART1_TX,ITC_PRIORITYLEVEL_0);
  ITC_SetSoftwarePriority(ITC_IRQ_UART1_RX,ITC_PRIORITYLEVEL_0);
  
  
  ITC_SetSoftwarePriority(ITC_IRQ_TIM2_OVF,ITC_PRIORITYLEVEL_2);
  ITC_SetSoftwarePriority(ITC_IRQ_TIM3_OVF,ITC_PRIORITYLEVEL_2);
  
  //выставление приоритетов внешних прерываний в среднее значение
  ITC_SetSoftwarePriority(ITC_IRQ_PORTA,ITC_PRIORITYLEVEL_1);
  ITC_SetSoftwarePriority(ITC_IRQ_PORTC,ITC_PRIORITYLEVEL_1);
  ITC_SetSoftwarePriority(ITC_IRQ_PORTD,ITC_PRIORITYLEVEL_1);
  ITC_SetSoftwarePriority(ITC_IRQ_PORTE,ITC_PRIORITYLEVEL_1);
  
  /*Понижение приоритета внешнего прерывания порта Е, для того, чтобы была возможность вызывать
  задержку по таймеру в прерывании по нажатию кнопок КК0-КК2*/
  //ITC_SetSoftwarePriority(ITC_IRQ_PORTE, ITC_PRIORITYLEVEL_0);
}

void Delay(__IO uint32_t nTime)
{
  TimingDelay = nTime;
  

  //Ждать пока значение задержки не достигло 0
  while (TimingDelay != 0);
  
  
}

//Декремент значения задержки в мс
void TimingDelay_Decrement(void)
{
  TimingDelay = TimingDelay > 0 ? (TimingDelay - 1) : TimingDelay; 
}

//инициализация данных МПК
void MpkDataInit(MpkDataTypeDef *mpkData)
{
  
  mpkData->State = STATE_CONNECT_TO_PU;
  mpkData->UserCommand = COMMAND_RETURN0;
  mpkData->PrevUserCommand = COMMAND_NONE;
  GetFlashStoredCommand(mpkData);                                               //вычитывание сохраненной команды пользователя 
  
}

//вычитывание сохраненной команды пользователя 
static void GetFlashStoredCommand(MpkDataTypeDef *mpkData)
{  
  UserCommandTypeDef readedCommand = ReadPuCommandFromFlash();                  //вычитывание
  
  for(uint8_t cnt=0; cnt < sizeof(CorrectCommands); cnt++)                      //проверка корректности вычитанной команды
  {
    if(readedCommand == (CorrectCommands[cnt]))                                 //если команда верная, вернуть ее
    {
      
      mpkData->UserCommand = readedCommand;
      if((uint8_t)GPIO_ReadInputPin(STAT_PORT,STAT_PIN) > 0)                    //если питания на 2м канале нет, то запомнить, какая команда была сохранена
      {                   
        mpkData->BeforePowerCh2OnCommand = readedCommand;
      }      
      return;
    }
  }
  WritePuCommandToFlash(COMMAND_RETURN0);                                       //записать по умолчанию команду
  mpkData->UserCommand = ReadPuCommandFromFlash();                              //вычитать команду повторно
  if((uint8_t)GPIO_ReadInputPin(STAT_PORT,STAT_PIN) > 0)                        //если питания на 2м канале нет, то запомнить, какая команда была сохранена
  {                   
     mpkData->BeforePowerCh2OnCommand = readedCommand;
  } 
}

//запись команды для ПУ в энергонезависимую память
void WritePuCommandToFlash(UserCommandTypeDef command)
{   
    FLASH_Unlock(FLASH_MEMTYPE_DATA);                                           //разблокирование FLASH
    FLASH_EraseByte(FLASH_COMMAND_ADDRESS);                                     //стирание байта 
    FLASH_ProgramByte(FLASH_COMMAND_ADDRESS, (uint8_t)command);                 //программирование байта
    FLASH_Lock(FLASH_MEMTYPE_DATA);                                             //блокирока FLASH
  
}

//вычитвыание команды по адресу в EEPROM
UserCommandTypeDef ReadPuCommandFromFlash()
{
  return (UserCommandTypeDef)(FLASH_ReadByte(FLASH_COMMAND_ADDRESS));
}

