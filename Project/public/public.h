#ifndef PUBLIC_H
#define PUBLIC_H

#include "stm8s.h"

/*Файл с определениями, общими для ПУ и МПК*/

uint8_t CheckCommand(const char* command, char* pBuffer2);
uint8_t CheckBuffer(const char* command, char* pBuffer,uint16_t charNum);
uint8_t Buffercmp(char* pBuffer1, char* pBuffer2, uint16_t BufferLength);
void InitBeeperPin(void);
void BeepSetToneAndStart(BEEP_Frequency_TypeDef tone);
uint8_t CheckCircularBuffer(const char* command, char* startBuf, uint16_t bufSize, uint16_t crIndex);

#define BT_COMMAND_IS_PU_REQ "AT+PU?\r"                                           //строка AT-команды запроса, является ли устройство ПУ
#define BT_COMMAND_IS_PU_REQ_NO_CR "AT+PU?"

#define BT_COMMAND_IS_PU_ANSWER_YES "AT+YES"                                    //строка ответа на команду запроса, если устройство ПУ
#define BT_COMMAND_IS_PU_ANSWER_NO "AT+NOO"                                        //строка ответа на команду запроса, если устройство не ПУ       
#define BT_COMMAND_RESET "AT+RESET"                                             //AT-команда сброса модуля Bluetooth
#define BT_RESPONSE_OK "OK"                                                     //положительный ответ на AT-команду
#define BT_COMMAND_MIN_LEN 3                                                    //минимальная длина команды

#define BT_COMMAND_PLUS90 "AT+POS90"                                            //AT-команда поворота платформы в +90
#define BT_COMMAND_PLUS90_CR "AT+POS90\r"                                       //AT-команда поворота платформы в +90
#define BT_COMMAND_MINUS90 "AT+NEG90"                                           //AT-команда поворота платформы в -90
#define BT_COMMAND_MINUS90_CR "AT+NEG90\r"                                      //AT-команда поворота платформы в -90
#define BT_COMMAND_ZERO "AT+ZERO"                                               //AT-команда поворота платформы в 0
#define BT_COMMAND_ZERO_CR "AT+ZERO\r"                                          //AT-команда поворота платформы в 0
#define BT_COMMAND_CATCH "AT+CATCH"                                             //AT-команда захвата объекта
#define BT_COMMAND_CATCH_CR "AT+CATCH\r"                                        //AT-команда захвата объекта
#define BT_COMMAND_FREE "AT+FREE"                                               //AT-команда освобождения объекта
#define BT_COMMAND_FREE_CR "AT+FREE\r"                                               //AT-команда освобождения объекта

#define BT_ACK_WAIT_MS 2000                                                     //таймаут ожидания ответа по BLUETOOTH
#define BT_MODULE_RESTART_TIME_MS 200                                           //время рестарта модуля Bluetooth в мс

#define BEEPER_AFR7_MASK 0x80                                                   //маска бита переназначения пина бипера для установки в опционный байт в EEPROM

typedef enum
{
  COMMAND_NONE,                                                                 //нет команды
  COMMAND_PLUS90DEG,                                                            //+90
  COMMAND_MINUS90DEG,                                                           //-90
  COMMAND_RETURN0,                                                              //0
  COMMAND_CATCH_OBJECT,                                                         //захват объекта
  COMMAND_FREE_OBJECT                                                           //высвобождение объекта
}UserCommandTypeDef;                                                            //пользовательские команды

#endif

 