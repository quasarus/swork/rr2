#include "main.h"
#include "stm8s_it.h"
#include "periph_init.h"
#include "stm8s.h"
#include "bluetooth_control.h"
#include "string.h"
#include "public.h"

//проверка команды в буфере
uint8_t CheckCommand(const char* command, char* pBuffer2)
{
  //если значение длины меньше минимального - вернуть ошибку
  if(strlen(command) < BT_COMMAND_MIN_LEN)
  { return 2;} 
  //сравнение буфера побайтно с строкой команды
  for(uint16_t cnt = 0; cnt < strlen(command); cnt++)
  {
    if(command[0] == pBuffer2[cnt])
    {      
      if(strncmp(command,&pBuffer2[cnt],strlen(command)) == 0)
      {
        return 0;
      }
      else
      {
        continue;
      }
    }
  }
  return 1;
    
}

uint8_t CheckBuffer(const char* command, char* pBuffer,uint16_t charNum)
{
  size_t commandLen = strlen(command);  
  uint16_t currentCommandCharNum = 0;
  
  if(commandLen > charNum)
  {
    return 2;
  }   
  for(uint16_t cntCharToCmp = 0;cntCharToCmp < charNum ; cntCharToCmp++)
  {
    if(pBuffer[cntCharToCmp] == command[currentCommandCharNum])
    {
      currentCommandCharNum++;
      if(currentCommandCharNum == commandLen)
      {
        return 0;
      }
    }
    else
    {
      currentCommandCharNum = 0;
    }
  }
  return 1;  
}

uint8_t CheckCircularBuffer(const char* command, char* startBuf, uint16_t bufSize, uint16_t crIndex)
{
  size_t commandLen = strlen(command);
	
  char* pEndBuf = (startBuf + (bufSize - 1));
  char* pSymInBuf = (crIndex == 0) ? pEndBuf : (startBuf + (crIndex - 1));
	
  while (commandLen--)
  {	
    if (command[commandLen] != *pSymInBuf)
    {
	return 1;
    }
    pSymInBuf = (pSymInBuf == startBuf) ? pEndBuf : (pSymInBuf - 1);
  }
  return 0;
}

 

//сравнение двух буфферов
uint8_t Buffercmp(char* pBuffer1, char* pBuffer2, uint16_t BufferLength)
{
    while (BufferLength--)
    {
        if (*pBuffer1 != *pBuffer2)
        {
            return 2;
        }

        pBuffer1++;
        pBuffer2++;
    }

    return 0;
}

//инициализация пина бипера
void InitBeeperPin(void)
{
  //выставление флага AFR7 в рег-ре OPT2 для того, чтобы альтернативная ф-ция порта PD4 была - выход для beeper
  if((OPT->OPT2 & BEEPER_AFR7_MASK) == 0)
  {
    volatile uint8_t opt2State = OPT->OPT2;                                     //вычитать хранящееся в опционном байте 2 значение
    uint16_t opt2Addr = (uint16_t)((uint16_t*)(&OPT->OPT2));                    //адрес опционного байта 2
    opt2State |= BEEPER_AFR7_MASK;                                              //наложение маски на значение опц. байта2
    //запись в память значения
    FLASH_Unlock(FLASH_MEMTYPE_DATA);
    FLASH_EraseOptionByte(opt2Addr);
    FLASH_ProgramOptionByte(opt2Addr, opt2State);
    FLASH_Lock(FLASH_MEMTYPE_DATA);   
  } 
}

//установить тон бипера и запустить бипер
void BeepSetToneAndStart(BEEP_Frequency_TypeDef tone)
{
#ifdef USE_BEEPER
  BEEP_DeInit();
  BEEP_Init(tone);
  BEEP_Cmd(ENABLE);
#endif
}
